<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *                Represents a cluster of contacts.
     */
    class ContactCluster {

        /**
         * @var string
         *
         *                        Identifier of the cluster.
         */
        public $ClusterId;

        /**
         * @var double
         *
         *                        Latitude of the cluster position.
         */
        public $Latitude;

        /**
         * @var double
         *
         *                        Longitude of the cluster position.
         */
        public $Longitude;

        /**
         * @var int
         *
         *                        Count of addresses in the cluster.
         */
        public $Count;

        /**
         * @var \de\cas\gw\server\geodata\types\InitialMapView
         *
         *                        Region in which all items in the cluster are located. Used to zoom in after double-clicking the cluster.
         */
        public $ZoomMapView;

        /**
         * @var string
         *
         *                        Link to be opened to display all items of the cluster in a listview.
         */
        public $ListLink;

        /**
         * @var int
         *
         *                        Count of all records in the cluster.
         */
        public $AggrItemCount;

        /**
         * @var string
         *
         *                        Aggregated value of the cluster as localized string. This value gets displayed in the clusters mouseover-popup in the map.
         */
        public $AggrValue;

        /**
         * @var double
         *
         *                        Aggregated value of the cluster as double value.
         */
        public $AggrValueDouble;

        /**
         * @var string
         *
         *                        Guid to be passed to the popUpDetailRequest.
         */
        public $PopupDetailId;

        /**
         * @var int
         *
         *                        Id of the ClusterCategory of the cluster.
         */
        public $ClusterCategoryId;

        /**
         * @var int
         *
         *                        Radius of the cluster in pixels.
         */
        public $Radius;

    }

}
