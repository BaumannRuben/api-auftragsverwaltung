<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     */
    class ImportVCardExceptionContainer {

        /**
         * @var int
         *
         *                The index of the vcard that caused the exception.
         */
        public $vCardIndex;

        /**
         * @var string
         *
         *                The string content of the vcard that caused the
         *                exception.
         */
        public $vcardContent;

        /**
         * @var string
         *
         *                The exception code indicating the type of failure.
         */
        public $exceptionCode;

        /**
         * @var string
         *
         *                The exception message from the catched exception.
         */
        public $exceptionMessage;

    }

}
