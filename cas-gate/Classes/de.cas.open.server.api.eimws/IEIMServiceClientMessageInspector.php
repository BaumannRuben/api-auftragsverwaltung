<?php

namespace de\cas\open\server\api\eimws {

    /**
     * IEIMServiceClientMessageInspector
     *
     * @package de\cas\open\server\api
     * @subpackage eimws
     */
    interface IEIMServiceClientMessageInspector {

        public function beforeSendRequest(&$function_name, array &$arguments, array &$options, array &$input_headers);

        public function afterReceiveReply(&$result, array &$output_headers);
    }
}
