<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the \de\cas\open\server\business\BusinessOperation
     *				that validates if the given password is valid.
     * 				Corresponding \de\cas\open\server\api\types\RequestObject: ValidateNewPasswordRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ValidateNewPasswordRequest
     */
    class ValidateNewPasswordResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    True if new password is valid.
         */
        public $valid;

        /**
         * @var array
         *
         *                    Contains translated error messages if property 'valid' is false.
         */
        public $errorMessages;

    }

}
