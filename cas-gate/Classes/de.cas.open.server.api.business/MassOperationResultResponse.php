<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject that transports failures.
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class MassOperationResultResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the failures that occurred while the practical
         *										actions were created.
         */
        public $failures;

    }

}
