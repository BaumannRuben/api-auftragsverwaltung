<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *                \de\cas\open\server\api\types\ResponseObject: Returns the contacts in the circumcircle.
     *                Corresponding \de\cas\open\server\api\types\RequestObject: GetGeoDataContactsInAreaRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGeoDataContactsInAreaRequest
     */
    class GetGeoDataContactsInAreaResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\geodata\types\AreaSearchResult
         *
         *                                        Sets/Returns the contactsInArea result.
         */
        public $AreaSearchResult;

    }

}
