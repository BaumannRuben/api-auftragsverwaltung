<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the distribution lists that
     *        contain a given address record.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetListsByItemResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetListsByItemResponse
     */
    class GetListsByItemRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the address record.
         */
        public $AddressGGUID;

    }

}
