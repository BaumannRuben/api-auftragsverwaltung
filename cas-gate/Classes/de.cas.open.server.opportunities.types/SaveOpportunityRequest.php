<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\RequestObject for the businessoperation that returns all
     *		    opportunitypositions to a given opportunity.
     *		    Corresponding \de\cas\open\server\api\types\ResponseObject: GetOpportunityPositionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetOpportunityPositionsResponse
     */
    class SaveOpportunityRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Sets/Returns the opportunity that shall be saved.
         */
        public $opportunity;

        /**
         * @var array
         *
         *										Returns the opportunitypositions that shall be saved.
         */
        public $positionsToAdd;

        /**
         * @var array
         *
         *										Returns the list of GGUIDs of opportunitypositions that
         *										shall be deleted.
         */
        public $gguidsOfPositionsToDelete;

        /**
         * @var array
         *
         *										Returns the columns used to sort the result (must be of
         *										same size as {@link #positionsOrderByTypes}).
         */
        public $positionsOrderByColumns;

        /**
         * @var array
         *
         *										Returns the directions used to sort the result (must be
         *										of same size as {@link #positionsOrderByColumns}).
         */
        public $positionsOrderByTypes;

    }

}
