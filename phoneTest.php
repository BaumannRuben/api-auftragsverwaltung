<?php
require_once dirname(__FILE__) . "/cas-gate/Erp_CASGate.php";
use de\cas\open\server\addressesvalidation\types\FormatPhoneNumberRequest;
function formatPhoneNumber($unformattedPhoneNumber, $countryCode = "de") {
    $formatPhoneNumberRequest = new FormatPhoneNumberRequest();
    $formatPhoneNumberRequest->phoneNumber = $unformattedPhoneNumber;
    $formatPhoneNumberRequest->countryCode = $countryCode;
    //     print_r($formatPhoneNumberRequest);
    $phoneNumberResponse = HochwarthIT_CASGate::getEIMInterface()->execute($formatPhoneNumberRequest);
    
    return $phoneNumberResponse;
}

print_r(formatPhoneNumber("072614074994", 'de'));

use de\cas\open\server\addressesvalidation\types\GuessSalutationRequest;
function guessSalutation($title, $name, $christianname, $preferredLanguage = "de") {
    $guessSalutationRequest = new GuessSalutationRequest();
    $guessSalutationRequest->title = $title;
    $guessSalutationRequest->name = $name;
    $guessSalutationRequest->christianname = $christianname;
    $guessSalutationRequest->preferredLanguage = $preferredLanguage;
    $guessSalutationResponse = HochwarthIT_CASGate::getEIMInterface()->execute($guessSalutationRequest);
    
    return $guessSalutationResponse;
}