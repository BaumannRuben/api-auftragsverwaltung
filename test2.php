<pre>
<?PHP
use de\cas\open\server\addresses\types\GetImageForContactResponse;

include 'cas-gate/CASGate.php';

/* Datenbankverbindung herstellen */
$con = new mysqli('192.168.40.10', 'root', 'root', 'hochwarth-zeit');
$con->set_charset('utf8');
if ($con->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $con->connect_error);
} else {
    echo "Verbindung erfolgreich hergestellt!<br><br>";
}

/* Datenbankverbindung herstellen */
$con2 = new mysqli('localhost', 'root', '', 'test1');
$con2->set_charset('utf8');
if ($con->connect_errno) {
    die("Verbindung 2 fehlgeschlagen: " . $con->connect_error);
} else {
    echo "Verbindung 2 erfolgreich hergestellt!<br><br>";
}


$anzAvKunden = 0;
$anzAvKundenNr = 0;
$anzApInsert = 0;
$anzApUpdate = 0;
$anzPicKunden = 0; 


$sqlkunden = $con->query('SELECT * FROM tblkunden;');
$avkunden = $sqlkunden->fetch_all(MYSQLI_ASSOC);
foreach ($avkunden as $avkunde) {
	$anzAvKunden++;
	if(!empty($avkunde['KundeNr'])) {
	    $anzAvKundenNr++;
	    $option_knd['conditions'] = array('HOCHWARTH_KUNDENNUMMER' => $avkunde['KundeNr'], 'AND GWISCOMPANY' => 1, 'AND GWISCONTACT' => 0);
	    $kunde = HochwarthIT_CASGate::getAdressDao()->find('all', $option_knd);
	    if(!empty($kunde)) {
	       echo "Kunde: ".$kunde[0]['COMPNAME']['value']."<br>";
	       
	       //Logo des Kunden �bertragen
	       if(!empty($kunde[0]['GWIMAGEGUID']['value'])) {
	           $image = new GetImageForContactResponse();
	           $image = HochwarthIT_CASGate::getAdressDao()->downloadPicture($kunde[0]['GGUID']['value']);
	           $imgData = base64_encode($image->ImageBytes);
	           $imgExtension = $image->ImageFileExtension;
	           if($avkunde['Logo']!=$imgData) {
	               $sql_update_pic = "UPDATE tblkunden SET Logo ='$imgData' Logotyp ='$imgExtension' "
	                                ."WHERE KundeNr = ".$kunde[0]['HOCHWARTH_KUNDENNUMMER']['value'].";";
	               $con->query($sql_update_pic);
	               $anzPicKunden++;
	           }
	       }
	       
	       //Ansprechpartner laden
	       $option_ap['conditions'] = array('PRIMARYORGANISATION' => '0x'.$kunde[0]['GGUID']['value'], 'AND GWISCOMPANY' => 0, 'AND GWISCONTACT' => 1);
	       $ansprechpartner = HochwarthIT_CASGate::getAdressDao()->find('all', $option_ap);
	       for($i=0;$i<sizeof($ansprechpartner);$i++) {
	           //Existenz des AP in der AV pr�fen
	           $sql_query = "SELECT * FROM tblkunden_ap WHERE `CAS-ID` = '".$ansprechpartner[$i]['GGUID']['value']."';";
	           $sqlap = $con2->query($sql_query);
               $ap = $sqlap->fetch_all(MYSQLI_ASSOC);
	           if(empty($ap)) {
	               //existiert noch nicht in der AV, Neuanlage
	               $sql_insert = "INSERT INTO tblkunden_ap (KundeNr, `CAS-Kunden-ID`, `CAS-ID`, Vorname, Name, Telefon, Mobil, Mail) VALUES ('"
	                           .$kunde[0]['HOCHWARTH_KUNDENNUMMER']['value']."', '" 
	                           .$kunde[0]['GGUID']['value']."', '"
	                           .$ansprechpartner[$i]['GGUID']['value']."', '"
	                           .$ansprechpartner[$i]['CHRISTIANNAME']['value']."', '"
	                           .$ansprechpartner[$i]['NAME']['value']."', '"
	                           .$ansprechpartner[$i]['PHONEFIELDSTR4']['value']."', '"
	                           .$ansprechpartner[$i]['PHONEFIELDSTR2']['value']."', '"
	                           .$ansprechpartner[$i]['MAILFIELDSTR1']['value']."');";
	               //echo $sql_insert."<br>";
	               $con2->query($sql_insert);
	               echo "AP-Neuanlage: ".$ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."<br>";
	               $anzApInsert++;
	           }
	           else {
	               //existiert, Aktualisierung
	               $sql_update = "UPDATE tblkunden_ap SET "
	                    ."Vorname ='".$ansprechpartner[$i]['CHRISTIANNAME']['value']."', "
	                    ."Name ='".$ansprechpartner[$i]['NAME']['value']."', "
	                    ."Telefon ='".$ansprechpartner[$i]['PHONEFIELDSTR4']['value']."', "
	                    ."Mobil ='".$ansprechpartner[$i]['PHONEFIELDSTR2']['value']."', "
	                    ."Mail ='".$ansprechpartner[$i]['MAILFIELDSTR1']['value']."' "
	                    ."WHERE KundeNr = ".$kunde[0]['HOCHWARTH_KUNDENNUMMER']['value'].";";
	               //echo $sql_update."<br>";
	               $con2->query($sql_update);
	               echo "AP-Aktualisierung: ".$ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."<br>";
	               $anzApUpdate++;
	           }
	       }
	    }
	}
}

$datei = fopen("logs\\protokoll_".date("Y-m-d-H-i-s").".txt","w");
echo "Kunden: ".$anzAvKunden."<br>";
echo "Kunden mit KundeNr: ".$anzAvKundenNr."<br>";
echo "AP-Neuanlage: ".$anzApInsert."<br>";
echo "AP-Aktualisierung: ".$anzApUpdate."<br>";
fwrite($datei,  "Kunden: $anzAvKunden\r\n");
fwrite($datei,  "Kunden mit KundeNr: $anzAvKundenNr\r\n");
fwrite($datei,  "Kunden ohne Kuerzel: $anzKundenOhneKuerzel\r\n");
fwrite($datei,  "AP-Neuanlage: $anzApInsert\r\n");
fwrite($datei,  "AP-Aktualisierung: $anzApUpdate\r\n");
fwrite($datei,  "Kunden mit Bild: $anzPicKunden\r\n");
fclose($datei);



$con->close();
$con2->close();
?>
</pre>


