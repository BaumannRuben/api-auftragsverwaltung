<?php
require_once dirname(__FILE__)."/SecurityTokenProvider.php";
require_once dirname(__FILE__)."/Dao/EventDao.php";
require_once dirname(__FILE__)."/Dao/AddressDao.php";
require_once dirname(__FILE__)."/Dao/ProductGroupDao.php";
require_once dirname(__FILE__)."/Dao/AppointmentDao.php";
require_once dirname(__FILE__)."/Dao/RegistrationDao.php";
require_once dirname(__FILE__)."/Dao/BillDao.php";
require_once dirname(__FILE__)."/Dao/BillPosDao.php";
require_once dirname(__FILE__)."/Dao/LicenseDao.php";
require_once dirname(__FILE__)."/Dao/TaskDao.php";
require_once dirname(__FILE__)."/Dao/DocumentDao.php";
require_once dirname(__FILE__)."/CASGate/InputAssistance/Address/Type.php";

class HochwarthIT_CASGate{

    static function getEIMInterface(){
        return SecurityTokenProvider::getInstance()->getEIMInterface();
    }

    static function getAvailableObjects(){
        return self::getEIMInterface()->getAvailableObjectNames();
    }

    static function getAdressDao(){
        return AddressDao::getInstance();
    }

    static function getProductGroupDao(){
        return ProductGroupDao::getInstance();
    }

    static function getEventDao(){
        return EventDao::getInstance();
    }

    static function getAppointmentDao(){
        return AppointmentDao::getInstance();
    }

    static function getRegistrationDao(){
        return RegistrationDao::getInstance();
    }

    static function getBillDao(){
        return BillDao::getInstance();
    }

    static function getBillPosDao(){
        return BillPosDao::getInstance();
    }

    static function getLicenseDao(){
        return LicenseDao::getInstance();
    }
    static function getTaskDao(){
        return TaskDao::getInstance();
    }
    static function getDocumentDao(){
        return DocumentDao::getInstance();
    }

}