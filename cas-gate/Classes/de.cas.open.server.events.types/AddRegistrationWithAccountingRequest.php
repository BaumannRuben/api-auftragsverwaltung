<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Adds a new registration for an event
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: AddRegistrationWithAccountingResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AddRegistrationWithAccountingResponse
     */
    class AddRegistrationWithAccountingRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    GUID for the participant address
         */
        public $participantGUID;

        /**
         * @var string
         *
         *                    GUID for the registerer address
         */
        public $registererGUID;

        /**
         * @var string
         *
         *                    GUID for the payment recipient address
         */
        public $payRecipientGUID;

        /**
         * @var string
         *
         *                    GUID for the event
         */
        public $eventGUID;

        /**
         * @var array
         *
         *                    list of appointment guids
         */
        public $appointmentGUIDs;

        /**
         * @var array
         *
         *                    list of accounting position guids
         */
        public $accountingPosGuids;

        /**
         * @var string
         *
         *                    State of the registration
         */
        public $registrationstate;

        /**
         * @var int
         *
         *                    Number of companions
         */
        public $numCompanions;

        /**
         * @var unknown
         *
         *                    date of the registration
         */
        public $registrationDate;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    Additional fields and values
         */
        public $AdditionalFieldsAndValues;

    }

}
