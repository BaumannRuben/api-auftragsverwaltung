<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: TODO: Purpose of the business logic. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: GetJournalResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetJournalResponse
     */
    class GetJournalRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Gets/Sets the GGUID of the \de\cas\open\server\api\types\DataObject whose journal entries
         *										shall be returned.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $GGUID;

        /**
         * @var string
         *
         *										Sets/Returns the type of the \de\cas\open\server\api\types\DataObject whose journal entries
         *										shall be returned.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $objectType;

        /**
         * @var unknown
         *
         *                    Sets/Returns the first record that
         *                    shall be returned, that is the offset
         *                    (<strong>zero-based</strong>)
         *                    of the first record in the result of the database
         *                    query. If it is <code>null</code>,
         *                    the method assumes 0.
         */
        public $firstRecord;

        /**
         * @var unknown
         *
         *                    Sets/Returns the number of records that shall be
         *                    contained in the result (if the query
         *                    returns more rows than this).
         */
        public $recordCount;

    }

}
