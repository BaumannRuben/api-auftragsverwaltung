<?php
// This file has been automatically generated.

namespace de\cas\open\server\csvexport\types {

    /**
     * @package de\cas\open\server\csvexport
     * @subpackage types
     *Abstract base class for \de\cas\open\server\api\types\RequestObjects to export data as CSV.
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    abstract class AbstractExportAsCSVRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The charset of the exported data.
         */
        public $charset;

        /**
         * @var string
         *
         *                    The delimiter with which the fields will be
         *                    separated (; or ,). If nothing is set the semicolon will be
         *                    used.
         */
        public $delimiter;

        /**
         * @var string
         *
         *                    Possibility to restrict export to selected
         *                    fields.
         */
        public $selectedFields;

        /**
         * @var boolean
         *
         *                    Include or exclude the hidden fields (gguid
         *                    etc.).
         */
        public $includeHiddenFields;

        /**
         * @var boolean
         *
         *                    If <code>true</code> and the object type is taggable the tags will be exported too.
         */
        public $exportTags;

        /**
         * @var boolean
         *
         *                    If <code>true</code> 'GGUID' columns will be removed from the export.
         */
        public $removeGGUIDColumns;

    }

}
