<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        Looks up permission owners (users/groups/resources) using a given email address.
     */
    class FindPermissionOwnersByEmailRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The email address which will be used to find matching permission owners. The email will be matched in case insensitive manner.
         */
        public $emailAddress;

    }

}
