<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Retrieves all valid databases from the server.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetValidDatabasesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetValidDatabasesResponse
     */
    class GetValidDatabasesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
