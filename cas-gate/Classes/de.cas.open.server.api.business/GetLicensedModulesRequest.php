<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        RequestObject which returns all available licence module names of an user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetLicensedModulesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetLicensedModulesResponse
     */
    class GetLicensedModulesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
