<?php
// This file has been automatically generated.

namespace de\cas\gw\server\addresses\types {

    /**
     * @package de\cas\gw\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject:
     *        Corresponding \de\cas\open\server\api\types\RequestObject: SavePersonalNotesForAddressRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SavePersonalNotesForAddressRequest
     */
    class SavePersonalNotesForAddressResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
