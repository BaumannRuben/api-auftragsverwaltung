<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *		    DEPRECATED! Please use CheckOutFileRequest. \de\cas\open\server\api\types\RequestObject: Checks out a document from the server. Corresponding
     *		    \de\cas\open\server\api\types\ResponseObject: CheckOutFileAndSaveDocumentResponse
     *	@see CheckOutFileRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckOutFileAndSaveDocumentResponse
     */
    class CheckOutFileAndSaveDocumentRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Sets/Returns the corresponding document object\de\cas\open\server\api\types\DataObject.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $documentDataObject;

        /**
         * @var boolean
         *
         *										Indicates if the document shall be checked out for
         *										reading only, it won't be locked if set to true. If
         *										false, the document will be locked.
         */
        public $readOnly;

        /**
         * @var int
         *
         *										This property is used to request a specific version of
         *										the document. If this value is not set, the most recent
         *										version will be returned.
         */
        public $versionNumber;

    }

}
