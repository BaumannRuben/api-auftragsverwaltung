<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *Class representing a data object in the queried interval
     */
    class FreeBusyDataObject {

        /**
         * @var string
         *GGUID of the data object
         */
        public $GGUID;

        /**
         * @var string
         *Data object type
         */
        public $DataObjectType;

        /**
         * @var string
         *Keyword of the data object
         */
        public $Keyword;

        /**
         * @var unknown
         *Start time of the data object
         */
        public $StartTime;

        /**
         * @var unknown
         *End time of the data object
         */
        public $EndTime;

    }

}
