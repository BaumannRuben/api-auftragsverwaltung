<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				This bean contains a small subset of user or group information. It's used
     *				for lists of permission owners. Until now, it's intended to save changes to
     *				this list.
     */
    class PermissionsOwnerInfo {

        /**
         * @var string
         *GGUID of a permission owner.
         */
        public $GGUID;

        /**
         * @var int
         *
         *								An internal ID in the database for faster access.
         */
        public $id;

        /**
         * @var string
         *
         *								The name of the permission owner (case-sensitiv), i.e. "Robert
         *								Glaser"
         */
        public $name;

        /**
         * @var string
         *
         *								The username of the permission owner (case-sensitiv)
         */
        public $username;

        /**
         * @var string
         *
         *								The description of the permission owner.
         */
        public $description;

        /**
         * @var string
         *
         *								Type of the permissions owner (User, Resouce or Group).
         */
        public $userType;

        /**
         * @var boolean
         *
         *								Indicates if the permission owner is enabled.
         */
        public $enabled;

        /**
         * @var boolean
         *
         *								Indicates if the group will be shown in
         *								dialogs other than the administration
         *								dialog. This useful for groups that simply
         *								exist to specify permissions but are not
         *								to be chosen as a permission owner of a
         *								record.
         */
        public $hidden;

        /**
         * @var string
         *GUID of a permission owners domain.
         */
        public $DomainGUID;

        /**
         * @var string
         *Name of a permission owners domain.
         */
        public $DomainName;

        /**
         * @var boolean
         *Is the permission owners domain local.
         */
        public $DomainIsLocal;

    }

}
