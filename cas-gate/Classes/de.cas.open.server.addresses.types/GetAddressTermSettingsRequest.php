<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Requests the address term (AddressTerms) and salutation (AddressLetters) suggestions based on a country code.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetAddressTermSettingsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAddressTermSettingsResponse
     */
    class GetAddressTermSettingsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Country code. Eg.: DE, AT, CH
         */
        public $AddressFormat;

    }

}
