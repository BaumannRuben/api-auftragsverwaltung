<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Changes the state for a list of existing registrations.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: ChangeRegistrationStateForMultipleObjectsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ChangeRegistrationStateForMultipleObjectsResponse
     */
    class ChangeRegistrationStateForMultipleObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *                    List of registration GUIDs.
         */
        public $registrationGUIDs;

        /**
         * @var string
         *
         *                    New state of the registrations.
         */
        public $registrationstate;

    }

}
