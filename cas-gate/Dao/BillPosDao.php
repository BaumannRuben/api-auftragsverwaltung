<?php

require_once "AbstractDao.php";

use \de\cas\open\server\api\types\LinkObject;
use \de\cas\open\server\api\business\SaveLinkRequest;

class BillPosDao extends AbstractDao
{

    protected $objectType = "BELEGPOSITION";
    static private $instance = null;

    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }


}