<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Response object for change of the state for a list of existing registrations.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: ChangeRegistrationStateForMultipleObjectsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ChangeRegistrationStateForMultipleObjectsRequest
     */
    class ChangeRegistrationStateForMultipleObjectsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Returns the guids of the data objects that were updated.
         */
        public $succeeded;

        /**
         * @var array
         *
         *                    Returns the guids of the data objects that could not be updated.
         */
        public $failed;

    }

}
