<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *                Result object with additional information to be displayed in cluster-popups.
     */
    class PopupDetailFromTemplateResult extends \de\cas\gw\server\geodata\types\PopupDetailResult {

        /**
         * @var string
         *
         */
        public $StringContent;

    }

}
