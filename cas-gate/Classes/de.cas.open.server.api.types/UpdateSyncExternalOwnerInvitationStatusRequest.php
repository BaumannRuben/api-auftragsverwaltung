<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Request Object to update external owners invitations status of a record
     *        when ExchangeSync is active. You can not change other fields than the inviation status
     */
    class UpdateSyncExternalOwnerInvitationStatusRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Gets/Sets the tablename of the record.
         */
        public $tableName;

        /**
         * @var string
         *
         *                    Gets/Sets the Guid of the record.
         */
        public $tableGuid;

        /**
         * @var string
         *
         */
        public $caller;

        /**
         * @var string
         *
         */
        public $currentUserMailAddress;

        /**
         * @var array
         *
         */
        public $UpdateInvitationStatus;

    }

}
