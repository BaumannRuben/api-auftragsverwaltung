<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns a resource bundle. Corresponding \de\cas\open\server\api\types\RequestObject: GetResourceBundleRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetResourceBundleRequest
     */
    class GetResourceBundleResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\EIMResourceBundleTransferable
         *
         *										Sets/Returns the transferable version of
         *										EIMResourceBundle that matched the parameters passed in
         *										the request.
         */
        public $resourceBundle;

    }

}
