<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the \de\cas\open\server\business\BusinessOperation that returns an
     * 				\de\cas\open\server\datadefinition\types\ObjectTypeDefinition describing the database
     * 				schema of a dataobject type.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetObjectTypeDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\datadefinition\types\ObjectTypeDefinition
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetObjectTypeDefinitionResponse
     */
    class GetObjectTypeDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The objecttype.
         */
        public $objectType;

    }

}
