<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns a list of tags (user tags and user editable
     *				system tags) that are free of constraints. Corresponding \de\cas\open\server\api\types\RequestObject:
     *				GetConstraintFreeTagsForUserRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetConstraintFreeTagsForUserRequest
     */
    class GetConstraintFreeTagsForUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Gets the reference to the tagslist.
         */
        public $tags;

    }

}
