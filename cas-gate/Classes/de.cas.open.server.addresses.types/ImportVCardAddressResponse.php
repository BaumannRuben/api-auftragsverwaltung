<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> TODO [Albina.Pace]: Purpose of the business logic.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: ImportVCardAddressRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ImportVCardAddressRequest
     */
    class ImportVCardAddressResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    The GGUIDs of the imported ADDRESS data objects.
         */
        public $importedContactsGuids;

        /**
         * @var int
         *
         *                    The total number of records parsed from the
         *                    VCard.
         */
        public $totalNumberOfRecords;

        /**
         * @var array
         *
         *                    Exception codes indicating errors that occured
         *                    during the import.
         */
        public $failures;

    }

}
