<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     */
    class AccountingPosition {

        /**
         * @var array
         *
         */
        public $children;

        /**
         * @var string
         *
         */
        public $gguid;

        /**
         * @var string
         *
         */
        public $type;

        /**
         * @var string
         *
         */
        public $keyword;

        /**
         * @var double
         *
         */
        public $price;

        /**
         * @var double
         *
         */
        public $percent;

        /**
         * @var string
         *
         */
        public $notes;

        /**
         * @var boolean
         *
         */
        public $isGroup;

        /**
         * @var double
         *
         */
        public $vat;

        /**
         * @var boolean
         *
         */
        public $filterApplied;

        /**
         * @var boolean
         *
         */
        public $selected;

        /**
         * @var int
         *
         */
        public $sortOrder;

    }

}
