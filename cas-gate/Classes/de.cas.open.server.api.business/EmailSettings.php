<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Represents the user email settings (mailserver
     *        settings).
     */
    class EmailSettings {

        /**
         * @var string
         *
         *                Returns the email setting's primary key.
         */
        public $GGUID;

        /**
         * @var string
         *
         *                Returns the name.
         */
        public $name;

        /**
         * @var string
         *
         *                Returns the hostname of the outbox server.
         */
        public $mailHostOut;

        /**
         * @var string
         *
         *                Returns the hostname of the inbox server.
         */
        public $mailHostIn;

        /**
         * @var string
         *
         *                Returns the hosttype of the inbox server (f.e.
         *                pop3, imap).
         */
        public $mailHostInType;

        /**
         * @var int
         *
         *                Returns the port of the inbox server.
         */
        public $mailHostInPort;

        /**
         * @var int
         *
         *                Returns the port of the outbox server.
         */
        public $mailHostOutPort;

        /**
         * @var int
         *
         *                The number of emails that can be sent per minute using this email server.
         */
        public $mailOutMailsPerMinute;

        /**
         * @var string
         *
         *                Returns the username of the mail account. (for
         *                authentication)
         */
        public $mailUsername;

        /**
         * @var string
         *
         *                Returns the password of the mail account. (for
         *                authentication)
         */
        public $mailPassword;

        /**
         * @var string
         *
         *                Returns the name of the account owner.
         */
        public $senderName;

        /**
         * @var string
         *
         *                Returns the email address of this account.
         */
        public $senderAddress;

        /**
         * @var boolean
         *
         *                Indicates if authentication for outgoing mail
         *                access will be used.
         */
        public $mailOutUseAuth;

        /**
         * @var boolean
         *
         *                Indicates if the outbox server requires ssl.
         */
        public $mailOutUseSsl;

        /**
         * @var boolean
         *
         *                Indicates if the inbox server requires ssl.
         */
        public $mailInUseSsl;

        /**
         * @var boolean
         *
         *                Indicates if the inbox server supports START-TLS.
         */
        public $mailInUseStartTls;

        /**
         * @var boolean
         *
         *                Indicates if the outbox server supports START-TLS.
         */
        public $mailOutUseStartTls;

        /**
         * @var boolean
         *
         *                Log in to the inbox server, before the email will
         *                be sent.
         */
        public $popBeforeIn;

        /**
         * @var boolean
         *
         *                Indicates if this are the default emailesettings of
         *                the user.
         */
        public $defaultMailSettings;

        /**
         * @var string
         *
         *                Reply address
         */
        public $replyAddress;

        /**
         * @var int
         *
         *                Returns the oid of the user the email settings
         *                refer to.
         */
        public $userOid;

        /**
         * @var string
         *
         *                Returns the guid of the user the email settings
         *                refer to.
         */
        public $userGuid;

    }

}
