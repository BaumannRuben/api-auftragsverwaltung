<?php
// This file has been automatically generated.

namespace de\cas\gw\server\addresses\types {

    /**
     * @package de\cas\gw\server\addresses
     * @subpackage types
     *
     *        Returns the custom address configuration.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetCustomAddressConfigurationResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetCustomAddressConfigurationResponse
     */
    class GetCustomAddressConfigurationRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
