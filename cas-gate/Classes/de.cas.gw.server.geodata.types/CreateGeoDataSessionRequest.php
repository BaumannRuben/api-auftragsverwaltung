<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *                \de\cas\open\server\api\types\RequestObject: Starts a new geo session.
     *                Corresponding \de\cas\open\server\api\types\ResponseObject: CreateGeoDataSessionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CreateGeoDataSessionResponse
     */
    class CreateGeoDataSessionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\geodata\types\GeoDataSessionParams
         *
         */
        public $Settings;

    }

}
