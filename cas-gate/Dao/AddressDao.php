<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

//Upload Kontaktbild
use de\cas\open\server\addresses\types\SaveImageForContactRequest;
use de\cas\open\server\api\business\GetJournalRequest;
use de\cas\open\server\addresses\types\GetImageForContactRequest;

class AddressDao extends AbstractDao
{

    protected $objectType = "ADDRESS";
    static private $instance = null;

    /* @return AddressDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt eine neue Adresse / einen neuen Kontakt
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createAddress($addressdata)
    {
        $addressObj = $this->createObject();
        foreach ($addressdata as $field => $row) {
            $addressObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($addressObj);
    }

    /*
     *  Adressen, welcher mit einer bestimmter Adresse verknüpft sind
     */
    public function getLinkedAddresses($adr_gguid, $fields = '*')
    {
        $sql = "SELECT ".$fields
            . " FROM ADDRESS AS A"
            . " WHERE A.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid)";
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
        $result = HochwarthIT_CASGate::getEIMInterface()->query($sql);

        return $this->convertResultObject($result);
    }

    /*
     *  Listet alle Adressverknüpfungen
     */
    public function getAddressLinks()
    {
        $linkObj = new LinkObject();
        $linkObj->objectType1 = "ADDRESS";
        $linkObj->objectType2 = "ADDRESS";
        $linksReq = new GetLinksRequest();
        $linksReq->LinkObject = $linkObj;
        $linkResponse = HochwarthIT_CASGate::getEIMInterface()->execute($linksReq);

        return $linkResponse;
    }

    /*
     *  Listet alle Adressverknüpfungen
     */
    public function getSpecificLink($gguid, $key)
    {
        $linkObj = new LinkObject();
        $linkObj->objectType1 = "ADDRESS";
        $linkObj->objectType2 = "ADDRESS";
        $linkObj->GGUID1 = $gguid;
        $linkObj->attribute = new StdClass();
        $linkObj->attribute->key = $key;
        $linksReq = new GetLinksRequest();
        $linksReq->LinkObject = $linkObj;

        $linkResponse = HochwarthIT_CASGate::getEIMInterface()->execute($linksReq);

        return $linkResponse;
    }

    public function getLinkedGguids($gguid)
    {

        $linkedAddresses = null;
        $headquarterGguids = $this->getAddressGguidsForLinkDirection($gguid, "LEFT_TO_RIGHT");

        if($headquarterGguids){

            foreach ($headquarterGguids as $headquarterGguid) {
                $hqGguid = $headquarterGguid->GGUID2;

                $agencyGguids = $this->getAddressGguidsForLinkDirection($hqGguid, "RIGHT_TO_LEFT");

                foreach ($agencyGguids as $agencyGguid) {
                    $agGguid = $agencyGguid->GGUID2;
                    $linkedAddresses[$hqGguid][] = $agGguid;
                }
            }
        }
        else{

            $agencyGguids = $this->getAddressGguidsForLinkDirection($gguid, "RIGHT_TO_LEFT");

            if($agencyGguids){
                foreach ($agencyGguids as $agencyGguid) {
                    $agGguid = $agencyGguid->GGUID2;
                    $linkedAddresses[$gguid][] = $agGguid;
                }
            }
        }

        return $linkedAddresses;

    }

    public function getAddressGguidsForLinkDirection($gguid, $direction = "RIGHT_TO_LEFT")
    {
        $linkObj = new LinkObject();
        $linkObj->objectType1 = "ADDRESS";
        $linkObj->objectType2 = "ADDRESS";
        $linkObj->GGUID1 = $gguid;
        $linkObj->linkDirection = $direction;
        $linksReq = new GetLinksRequest();
        $linksReq->LinkObject = $linkObj;
        $linkResponse = HochwarthIT_CASGate::getEIMInterface()->execute($linksReq);

        return $linkResponse->links;

    }

    /**
     *  Erzeugt eine Verknüpfung zwischen zwei Adressen
     *
     *  @param $gguid1 String Filiale / Tochter
     *  @param $gguid2 String Zentrale / Mutter
     *  @param $attribute String L2UADRHIERARCHY --> Mutter-Tochter-Hierarchie
     */
    public function createLinkBetweenAddresses($gguid1, $gguid2, $attribute = null, $dir = "RIGHT_TO_LEFT")
    {
        $link = new LinkObject();
        $link->objectType1 = "ADDRESS";
        $link->GGUID1 = $gguid1;
        $link->objectType2 = "ADDRESS";
        $link->GGUID2 = $gguid2;
        $link->attribute = new StdClass();
        $link->attribute->key = $attribute;
        $link->isHierarchy = true;
        $link->linkDirection = $dir; //Right = Tochter, Left = Mutter

        $linkRequest = new SaveLinkRequest();
        $linkRequest->links = array($link);

        $linkResponse = HochwarthIT_CASGate::getEIMInterface()->execute($linkRequest);

        return $linkResponse;
    }

    /*
     *  Löscht eine Adressverknüpfung
     */
    public function deleteLinkBetweenAddresses($gguid)
    {
        $linkRequest = new DeleteLinkRequest();
        $linkRequest->linksToBeDeleted = array($gguid);
        $linkResponse = HochwarthIT_CASGate::getEIMInterface()->execute($linkRequest);

        return $linkResponse;
    }

    public function getGguidIfUniqueEmail($email){
        $options = array(
            "conditions" => array(
                "AND" => array(
                    "GWDEACTIVATED" => false,
                    "OR" => array(
                        "MAILFIELDSTR1" => $email,
                        "MAILFIELDSTR2" => $email,
                        "MAILFIELDSTR3" => $email,
                        "MAILFIELDSTR4" => $email,
                        "MAILFIELDSTR5" => $email,
                    )
                )
            )
        );
        $cas_customers = $this->find("list", $options);
        //wenn unique dann die GGUID zurückgeben
        if(count($cas_customers) === 1){
            reset($cas_customers);
            return key($cas_customers);
        }
        return false;
    }

    /* @return \de\cas\open\server\api\types\DataObject */
    public function checkDuplicate($firstname, $lastname, $birthday){
        $duplicate = $this->find("first",
            array(
                "conditions" => array(
                    "AND" => array(
                        "CHRISTIANNAME" => $firstname,
                        "NAME" => $lastname,
                        "BIRTHDAY" => $birthday
                    )
                )
            )
        );
        if($duplicate){
            return true;
        }
        else{
            return false;
        }
    }
    public function checkDuplicateCompany($compname, $firstname, $lastname){
        $duplicate = $this->find("first",
            array(
                "conditions" => array(
                    "AND" => array(
                        "COMPNAME" => $compname,
                        "CHRISTIANNAME" => $firstname,
                        "NAME" => $lastname                        
                    )
                )
            )
            );
        if($duplicate){
            return true;
        }
        else{
            return false;
        }
    }

    public function getLinkedRechnungsempfaenger($adr_gguid, $fields = '*')
    {
        $sql = "SELECT ".$fields
            . " FROM ADDRESS AS A"
            . " WHERE A.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid)"
            . " AND A.E_R_ABWREADR = 1";
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
        $result = HochwarthIT_CASGate::getEIMInterface()->query($sql);

        return $this->convertResultObject($result);
    }
	
	    //Upload Kontaktbild
    public function uploadPicture($path, $adr_gguid) {
    	$file = fopen($path, "rb");
       	$imageBytes = fread($file, filesize($path));
    	fclose($file);
    	$imageRequest = new SaveImageForContactRequest();
    	$imageRequest->AdressGGUID = "0x".$adr_gguid;
    	$imageRequest->ImageBytes = $imageBytes;
    	$imageRequest->ImageFileExtension = 'jpg';
    	$imageResponse = HochwarthIT_CASGate::getEIMInterface()->execute($imageRequest);
    }
    
    //Download Kontaktbild
    public function downloadPicture($adr_gguid) {
    	$imageRequest = new GetImageForContactRequest();
    	$imageRequest->AdressGGUID = "0x".$adr_gguid;
    	$imageResponse = HochwarthIT_CASGate::getEIMInterface()->execute($imageRequest);
    	
    	return $imageResponse;
    }
}

