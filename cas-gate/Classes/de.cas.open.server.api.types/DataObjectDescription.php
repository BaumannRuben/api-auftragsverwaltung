<?php

namespace de\cas\open\server\api\types {

    use \de\cas\open\server\datadefinition\types\ObjectTypeDependency;
    use \de\cas\open\server\api\types\FieldDescription;
    use \de\cas\open\server\api\types\DataObjectDescriptionTransferable;

    /**
     * A DataObjectDescription describes a specific type of a DataObject. In CAS Open, the
     * DataObjectDescription is user specific.
     *
     * @package de\cas\open\server\api
     * @subpackage types
     *
     * @see DataObjectDescriptionTransferable
     */
    class DataObjectDescription {

        /**
         * @var string
         */
        private $serviceID;

        /**
         * @var array
         */
        private $fieldMap = array();

        /**
         * @var string
         */
        private $descriptionType;

        /**
         * @var string
         */
        private $language;

        /**
         * @var boolean
         */
        private $taggable;

        /**
         * @var boolean
         */
        private $linkable;

        /**
         * @var boolean
         */
        private $usedInMixedList;

        /**
         * @var array
         */
        private $mandatoryFields = array();

        /**
         * @var array
         */
        private $systemFields = array();

        /**
         * @var array
         */
        private $recommendedFields = array();

        /**
         * @var array
         */
        private $nonHiddenFields = array();

        /**
         * @var array
         */
        private $tagGroupMap = array();

        /**
         * @var boolean
         */
        private $userSensitive;

        /**
         * @var string
         */
        private $client;

        /**
         * @var unknown
         */
        private $descriptionVersion;

        /**
         * @var boolean
         */
        private $rBinActivated;

        /**
         * @var boolean
         */
        private $templActivated;

        /**
         * @var \de\cas\open\server\datadefinition\types\ObjectTypeDependency
         */
        private $superiorTypeDependency;

        /**
         * @var array
         */
        private $dependentTypeDependencies;

        /**
         * @var int
         */
        private $dataObjectTypePermission;

        /**
         * @var unknown
         */
        private $primaryLinkParents;


        /**
         * @return string
         */
        public function getClient() {
            return $this->client;
        }

        /**
         * @param string $client
         * @return void
         */
        public function setClient($client) {
            $this->client = $client;
        }

        /**
         * @return string
         */
        public function getDescriptionType() {
            return $this->descriptionType;
        }

        /**
         * @param string $descriptionType
         * @return void
         */
        public function setDescriptionType($descriptionType) {
            $this->descriptionType = strtoupper($descriptionType);
        }

        /**
         * @return string
         */
        public function getServiceID() {
            return $this->serviceID;
        }

        /**
         * @param string $serviceID
         * @return void
         */
        public function setServiceID($serviceID) {
            $this->serviceID = $serviceID;
        }

        /**
         * @return boolean
         */
        public function getTaggable() {
            return $this->taggable;
        }

        /**
         * @param boolean $taggable
         * @return void
         */
        public function setTaggable($taggable) {
            $this->taggable = $taggable;
        }

        /**
         * @return boolean
         */
        public function getLinkable() {
            return $this->linkable;
        }

        /**
         * @param boolean $linkable
         * @return void
         */
        public function setLinkable($linkable) {
            $this->linkable = $linkable;
        }

        /**
         * @return boolean
         */
        public function getUsedInMixedList() {
            return $this->usedInMixedList;
        }

        /**
         * @param boolean $usedInMixedList
         * @return void
         */
        public function setUsedInMixedList($usedInMixedList) {
            $this->usedInMixedList = $usedInMixedList;
        }

        /**
         * @return boolean
         */
        public function getUserSensitive() {
            return $this->userSensitive;
        }

        /**
         * @param boolean $userSensitive
         * @return void
         */
        public function setUserSensitive($userSensitive) {
            $this->userSensitive = $userSensitive;
        }

        /**
         * @return unknown
         */
        public function getDescriptionVersion() {
            return $this->descriptionVersion;
        }

        /**
         * @param unknown $descriptionVersion
         * @return void
         */
        public function setDescriptionVersion($descriptionVersion) {
            $this->descriptionVersion = $descriptionVersion;
        }

        /**
         * @return \de\cas\open\server\datadefinition\types\ObjectTypeDependency
         */
        public function getSuperiorTypeDependency() {
            return $this->superiorTypeDependency;
        }

        /**
         * @param \de\cas\open\server\datadefinition\types\ObjectTypeDependency $superiorTypeDependency
         * @return void
         */
        public function setSuperiorTypeDependency($superiorTypeDependency) {
            $this->superiorTypeDependency = $superiorTypeDependency;
        }

        /**
         * @return array
         */
        public function getDependentTypeDependencies() {
            return $this->dependentTypeDependencies;
        }

        /**
         * @param array $dependentTypeDependencies
         * @return void
         */
        public function setDependentTypeDependencies($dependentTypeDependencies) {
            $this->dependentTypeDependencies = $dependentTypeDependencies;
        }

        /**
         * @return int
         */
        public function getDataObjectTypePermission() {
            return $this->dataObjectTypePermission;
        }

        /**
         * @param int $dataObjectTypePermission
         * @return void
         */
        public function setDataObjectTypePermission($dataObjectTypePermission) {
            $this->dataObjectTypePermission = $dataObjectTypePermission;
        }

        /**
         * @return string
         */
        public function getLanguage() {
            return $this->language;
        }

        /**
         * @param string $language
         * @return void
         */
        public function setLanguage($language) {
            $this->language = $language;
        }

        /**
         * @return array
         */
        public function getFieldMap() {
            return $this->fieldMap;
        }

        /**
         * @param array $fieldMap
         * @return void
         */
        public function setFieldMap(array $fieldMap) {
            $this->fieldMap = $fieldMap;
        }

        /**
         * @return array
         */
        public function getTagGroupMap() {
            return $this->tagGroupMap;
        }

        /**
         * @param array $tagGroupMap
         * @return void
         */
        public function setTagGroupMap(array $tagGroupMap) {
            $this->tagGroupMap = $tagGroupMap;
        }

        /**
         * @return array
         */
        public function getMandatoryFields() {
            return $this->mandatoryFields;
        }

        /**
         * @param array $mandatoryFields
         * @return void
         */
        public function setMandatoryFields(array $mandatoryFields) {
            $this->mandatoryFields = $mandatoryFields;
        }

        /**
         * @return array
         */
        public function getSystemFields() {
            return $this->systemFields;
        }

        /**
         * @param array $systemFields
         * @return void
         */
        public function setSystemFields(array $systemFields) {
            $this->systemFields = $systemFields;
        }

        /**
         * @return array
         */
        public function getRecommendedFields() {
            return $this->recommendedFields;
        }

        /**
         * @param array $recommendedFields
         * @return void
         */
        public function setRecommendedFields(array $recommendedFields) {
            $this->recommendedFields = $recommendedFields;
        }

        /**
         * @return array
         */
        public function getNonHiddenFields() {
            return $this->nonHiddenFields;
        }

        /**
         * @param array $nonHiddenFields
         * @return void
         */
        public function setNonHiddenFields(array $nonHiddenFields) {
            $this->nonHiddenFields = $nonHiddenFields;
        }

        /**
         * @return unknown
         */
        public function getPrimaryLinkParents() {
            return $this->primaryLinkParents;
        }

        /**
         * @param unknown $primaryLinkParents
         * @return void
         */
        public function setPrimaryLinkParents($primaryLinkParents) {
            $this->primaryLinkParents = $primaryLinkParents;
        }

        /**
         * @return boolean
         */
        public function getRBinActivated() {
            return $this->rBinActivated;
        }

        /**
         * @param boolean $rBinActivated
         * @return void
         */
        public function setRBinActivated($rBinActivated) {
            $this->rBinActivated = $rBinActivated;
        }

        /**
         * @return boolean
         */
        public function getTemplActivated() {
            return $this->templActivated;
        }

        /**
         * @param boolean $templActivated
         * @return void
         */
        public function setTemplActivated($templActivated) {
            $this->templActivated = $templActivated;
        }


        /**
         * Creates a new DataObjectDescription.
         *
         * @param string $type
         * @param array $fieldDescriptions
         * @param string $client
         * @param unknown $descriptionVersion
         * @param string $serviceID
         */
        public function __construct($type, $fieldDescriptions, $client, $descriptionVersion = NULL, $serviceID = NULL) {
            $this->descriptionType = strtoupper($type);
            $this->client = $client;
            $this->copyFieldsToMap($fieldDescriptions);
            $this->descriptionVersion = $descriptionVersion;
            $this->serviceID = $serviceID;
        }

        /**
         * Creates a DataObjectDescription from a DataObjectDescriptionTransferable.
         *
         * @param DataObjectDescriptionTransferable $transferable
         * @return DataObjectDescription
         */
        public static function fromDataObjectDescriptionTransferable(DataObjectDescriptionTransferable $transferable) {
            $dataObjectDescription = new static($transferable->descriptionObjectType,
                                                $transferable->fields,
                                                $transferable->client,
                                                $transferable->descriptionVersion,
                                                $transferable->serviceID);

            $dataObjectDescription->language = $transferable->language;
            $dataObjectDescription->userSensitive = $transferable->userSensitive;
            $dataObjectDescription->taggable = $transferable->taggable;
            $dataObjectDescription->linkable = $transferable->linkable;
            $dataObjectDescription->rBinActivated = $transferable->rBinActivated;
            $dataObjectDescription->templActivated = $transferable->templActivated;

            if ($transferable->dependentTypeDependencies !== NULL) {
                $dataObjectDescription->dependentTypeDependencies = array_values($transferable->dependentTypeDependencies);
            }
            $dataObjectDescription->superiorTypeDependency = $transferable->superiorTypeDependency;
            $dataObjectDescription->dataObjectTypePermission = $transferable->dataObjectTypePermission;
            $dataObjectDescription->primaryLinkParents = $transferable->primaryLinkParents;

            if ($transferable->localizedTagGroups !== NULL) {
                foreach ($transferable->localizedTagGroups as $locTagGroup) {
                    $dataObjectDescription->tagGroupMap[$locTagGroup->GGUID] = $locTagGroup;
                }
            }

            return $dataObjectDescription;
        }

        /**
         * @param array $fieldList
         * @return void
         */
        public function copyFieldsToMap(array $fieldList = NULL) {
            if ($fieldList === NULL)
                return;
            foreach ($fieldList as $field) {
                $fieldName = $field->name;
                $this->fieldMap[$fieldName] = $field;

                if (!empty($field->mandatory)) {
                    $this->mandatoryFields[] = $fieldName;
                }
                if (!empty($field->systemField)) {
                    $this->systemFields[] = $fieldName;
                }
                if (!empty($field->recommended)) {
                    $this->recommendedFields[] = $fieldName;
                }
                if (empty($field->hidden)) {
                    $this->nonHiddenFields[] = $fieldName;
                }
            }
        }

        /**
         * Clones a DataObjectDescription with all fields.
         *
         * @return void
         */
        public function __clone() {
            foreach ($this->fieldMap as $fieldName => &$field) {
                $field = clone $field;
            }
            foreach ($this->tagGroupMap as $tagGroupGGUID => &$tagGroup) {
                $tagGroup = clone $tagGroup;
            }

            $this->superiorTypeDependency = clone $this->superiorTypeDependency;
            if ($this->dependentTypeDependencies !== NULL) {
                foreach ($this->dependentTypeDependencies as &$dependentTypeDependency) {
                    $dependentTypeDependency = clone $dependentTypeDependency;
                }
            }
        }

        /**
         * @param string $fieldName
         * @return \de\cas\open\server\api\types\FieldDescription
         */
        public function getFieldDescription($fieldName) {
            $upperFieldName = strtoupper($fieldName);
            if (!isset($this->fieldMap[$upperFieldName])) {
                return NULL;
            }
            $field = $this->fieldMap[$upperFieldName];
            return $field;
        }

        /**
         * @return DataObjectDescriptionTransferable
         */
        public function toDataObjectDescriptionTransferable() {
            $transferable = new DataObjectDescriptionTransferable;
            $transferable->descriptionObjectType = $this->descriptionType;

            $transferable->language = $this->language;
            $transferable->userSensitive = $this->userSensitive;
            $transferable->client = $this->client;
            $transferable->descriptionVersion = $this->descriptionVersion;
            $transferable->taggable = $this->taggable;
            $transferable->linkable = $this->linkable;
            $transferable->rBinActivated = $this->rBinActivated;
            $transferable->templActivated = $this->templActivated;
            $transferable->serviceID = $this->serviceID;

            $transferable->fields = array_values($this->fieldMap);

            $transferable->localizedTagGroups = array_values($this->tagGroupMap);

            $transferable->superiorTypeDependency = $this->superiorTypeDependency;
            if ($this->dependentTypeDependencies !== NULL) {
                $transferable->dependentTypeDependencies = array_values($this->dependentTypeDependencies);
            }

            $transferable->dataObjectTypePermission = $this->dataObjectTypePermission;
            $transferable->primaryLinkParents = $this->primaryLinkParents;

            return $transferable;
        }

        /**
         * @param string $fieldName
         * @return boolean
         */
        public function containsField($fieldName) {
            $upperFieldName = strtoupper($fieldName);
            return isset($this->fieldMap[$upperFieldName]);
        }

        /**
         * @return array
         */
        public function allFields() {
            $fields = array_keys($this->fieldMap);
            return $fields;
        }

    }

}
