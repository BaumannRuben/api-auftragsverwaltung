<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        A container for a String list suggest value in a generic DataObject.
     */
    class StringListSuggestField extends \de\cas\open\server\api\types\Field {

        /**
         * @var array
         *The String values of this field
         */
        public $values;

    }

}
