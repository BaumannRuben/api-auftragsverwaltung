<pre>
<?PHP
include 'cas-gate/CASGate.php';
use de\cas\open\server\addresses\types\GetImageForContactResponse;

/* Datenbankverbindung herstellen */
$con = new mysqli('192.168.40.13', 'root', 'root', 'hochwarth-zeit');
$con->set_charset('utf8');
if ($con->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $con->connect_error);
} else {
    echo "Verbindung erfolgreich hergestellt!<br><br>";
}

$anzAvMA = 0;
$anzPicMA = 0;
$anzPicMAFail = 0; 
$year = date("Y");

$sqlma = $con->query("SELECT * FROM tblmitarbeiter WHERE Email IS NOT NULL AND Email != '' AND Ausscheiden >=$year;");
$avma = $sqlma->fetch_all(MYSQLI_ASSOC);
foreach ($avma as $mitarbeiter) {
    $anzAvMA ++;
    $option_ma['conditions'] = array('GWSTYPE' => 'Mitarbeiter', 'AND MAILFIELDSTR1' => $mitarbeiter['Email'], 'AND GWISCOMPANY' => 0, 'AND GWISCONTACT' => 1);
    $person = HochwarthIT_CASGate::getAdressDao()->find('first', $option_ma);
    if (!empty($person)) {
        echo "Mitarbeiter: " . $person['NAME']['value'] . ", " . $person['CHRISTIANNAME']['value'] . "<br>";
        // Bild des Mitarbeiters übertragen
        if (! empty($person['GWIMAGEGUID']['value'])) {
            $image = new GetImageForContactResponse();
            $image = HochwarthIT_CASGate::getAdressDao()->downloadPicture($person['GGUID']['value']);
            $imgData = base64_encode($image->ImageBytes);
            $imgExtension = $image->ImageFileExtension;
            if ($mitarbeiter['Bild'] != $imgData) {
                $sql_update_pic = "UPDATE tblmitarbeiter SET Bild ='" . $imgData . "', Bildtyp ='" . $imgExtension . "' " . "WHERE MitarbeiterNr = " . $mitarbeiter['MitarbeiterNr'] . ";";
                echo $sql_update_pic."<br>";
                if ($con->query($sql_update_pic)) {
                    $anzPicMA ++;
                } else {
                    $anzPicMAFail ++;
                }
            }
        }
    }
}

$datei = fopen("mitarbeiter_logs\\protokoll_".date("Y-m-d-H-i-s").".txt","w");
echo "MA: ".$anzAvMA."<br>";
echo "MA mit Bild: ".$anzPicMA."<br>";
echo "MA mit Bild Fehler: ".$anzPicMAFail."<br>";
fwrite($datei,  "MA: $anzAvMA\r\n");
fwrite($datei,  "MA mit Bild: $anzPicMA\r\n");
fwrite($datei,  "MA mit Bild Fehler: $anzPicMAFail\r\n");
fclose($datei);

$con->close();
?>
</pre>