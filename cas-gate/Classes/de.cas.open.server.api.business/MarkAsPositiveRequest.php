<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject to remove the false positive flag. Corresponding \de\cas\open\server\api\types\ResponseObject: MarkAsPositiveResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see MarkAsPositiveResponse
     */
    class MarkAsPositiveRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Contains the gguids of the records to be unmarked as
         *										false duplicates.
         */
        public $gguidList;

    }

}
