<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the \de\cas\open\server\business\BusinessOperation that returns an
     * 				\de\cas\open\server\datadefinition\types\ObjectTypeDefinition describing the database
     * 				schema of a dataobject type.
     * 				Corresponding \de\cas\open\server\api\types\RequestObject: GetObjectTypeDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\datadefinition\types\ObjectTypeDefinition
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetObjectTypeDefinitionRequest
     */
    class GetObjectTypeDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\datadefinition\types\ObjectTypeDefinition
         *
         *                    Describes the actual state of the objecttype.
         */
        public $objectTypeDefinition;

    }

}
