<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class StringListSuggestValueColumnMetaInformation extends \de\cas\open\server\datadefinition\types\SuggestValueColumnMetaInformation {

        /**
         * @var array
         *
         */
        public $value;

    }

}
