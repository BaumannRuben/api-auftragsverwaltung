<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the address term (AddressTerms) and salutation (AddressLetters) suggestions based on a country code.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetAddressTermSettingsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAddressTermSettingsRequest
     */
    class GetAddressTermSettingsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    The suggestions for the address term field. The key is the gender (M/W/MW), the value is the suggestion.
         */
        public $AddressTerms;

        /**
         * @var array
         *
         *                    The suggestions for the address salutation (ADDRESSLETTER) field. The key is the gender (M/W/MW), the value is the suggestion.
         */
        public $AddressLetters;

    }

}
