<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Removes items from a distribution list.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteItemsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteItemsResponse
     */
    class DeleteItemsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the distribution list record.
         */
        public $DistributionListGGUID;

        /**
         * @var array
         *
         *										GGUID of the address records.
         */
        public $AddressGGUIDs;

    }

}
