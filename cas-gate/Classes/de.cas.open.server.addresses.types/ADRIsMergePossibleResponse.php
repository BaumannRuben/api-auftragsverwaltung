<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject:
     *				Checks that a merge of an address with a duplicate is possible. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: ADRIsMergePossibleRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ADRIsMergePossibleRequest
     */
    class ADRIsMergePossibleResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Gets/Sets the reasons why a merge from DataObject 1 into 2 isn't possible.
         *										i.e. why it is not possible to delete 1. The reasons will refer to 1 as
         *										the SELECTED address.
         */
        public $negativeVoteReasons1Into2;

        /**
         * @var array
         *
         *										Gets/Sets the reasons why a merge from DataObject 2 into 1 isn't possible.
         *										i.e. why it is not possible to delete 2. The reasons will refer to 2 as
         *										the DUPLICATE address.
         */
        public $negativeVoteReasons2Into1;

    }

}
