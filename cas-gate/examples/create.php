<?php

#Including CAS-Gate
require __DIR__ . "/../CASGate.php";

$appointment = HochwarthIT_CASGate::getEIMInterface()->createObject('APPOINTMENT');
$appointment->setValue("Test Appointment", "KEYWORD", "STRING");
$appointment->setValue(FALSE, "ISPARTOFEVENT", "BOOLEAN");
$appointment = HochwarthIT_CASGate::getEIMInterface()->saveAndReturnObject($appointment);