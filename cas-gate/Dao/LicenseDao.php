<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

class LicenseDao extends AbstractDao
{
    protected $objectType = "XYZ_LIZENZ";
    static private $instance = null;

    /* @return AddressDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    public function createLinkToAddress($gguid1, $gguid2, $attribute = null, $dir = "LEFT_TO_RIGHT")
    {
        $link = new LinkObject();
        $link->objectType1 = "ADDRESS";
        $link->GGUID1 = $gguid1;
        $link->objectType2 = "XYZ_LIZENZ";
        $link->GGUID2 = $gguid2;
        $link->attribute = new StdClass();
        $link->attribute->key = $attribute;
        $link->isHierarchy = true;
        $link->linkDirection = $dir;

        $linkRequest = new SaveLinkRequest();
        $linkRequest->links = array($link);

        $linkResponse = HochwarthIT_CASGate::getEIMInterface()->execute($linkRequest);

        return $linkResponse;
    }

}