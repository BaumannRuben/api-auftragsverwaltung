<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: This operation is used to save the preferences of a
     *				user. The preferences 'preferredCountry', 'preferredLanguage' and 'timeZoneId'
     *				are handled seperatly from the other properties, which can be defined
     *				generically. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveUserPreferencesResponse Throws a BUSINESS.LINKED_ADDRESS_NOT_READABLE if
     *				the linked address record is not readable.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveUserPreferencesResponse
     */
    class SaveUserPreferencesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The preferred country that will be set for the
         *										currently logged in user. If it is not set it won't be
         *										altered.
         */
        public $preferredCountry;

        /**
         * @var string
         *
         *										The preferred language that will be set for the
         *										currently logged in user. If it is not set it won't be
         *										altered.
         */
        public $preferredLanguage;

        /**
         * @var string
         *
         *										The timezone-ID that will be set for the currently
         *										logged in user. If it is not set it won't be altered.
         */
        public $timeZoneId;

        /**
         * @var string
         *
         *										An address record can be linked to the user using this
         *										GUID.
         */
        public $addressRecordGuid;

        /**
         * @var array
         *
         *										The properties that should be saved for the currently
         *										logged in user.
         */
        public $propertyGroups;

    }

}
