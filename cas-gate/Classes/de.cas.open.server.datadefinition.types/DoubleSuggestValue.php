<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class DoubleSuggestValue extends \de\cas\open\server\datadefinition\types\SuggestValue {

        /**
         * @var double
         *
         */
        public $DisplayName;

    }

}
