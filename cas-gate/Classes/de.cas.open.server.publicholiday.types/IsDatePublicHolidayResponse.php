<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Checks if a given date
     *				is a public holiday in the given
     *				country/state/region. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: IsDatePublicHolidayRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see IsDatePublicHolidayRequest
     */
    class IsDatePublicHolidayResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Flag that indicates if the date in the
         *										corresponding
         *										request is a public holiday in the given
         *										country/state/region.
         */
        public $isHoliday;

    }

}
