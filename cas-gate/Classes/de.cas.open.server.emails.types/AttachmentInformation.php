<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        This element contains information about an e-mail attachment.
     */
    class AttachmentInformation {

        /**
         * @var string
         *
         */
        public $contentType;

        /**
         * @var string
         *
         */
        public $fileName;

        /**
         * @var unknown
         *
         */
        public $size;

    }

}
