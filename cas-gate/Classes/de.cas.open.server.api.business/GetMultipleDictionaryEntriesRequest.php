<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\RequestObject of the \de\cas\open\server\business\BusinessOperation
     *        that returns dictionary entries for multiple keys.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetMultipleDictionaryEntriesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetMultipleDictionaryEntriesResponse
     */
    class GetMultipleDictionaryEntriesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *                    The keys of the dictionary entries that shall be
         *                    returned.
         */
        public $keys;

    }

}
