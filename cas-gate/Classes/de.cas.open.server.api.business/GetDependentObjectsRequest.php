<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the \de\cas\open\server\api\types\DataObjects that
     *        depend from the \de\cas\open\server\api\types\DataObject
     *        which is identified by the given object type and GUID. Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: GetDependentObjectsResponse
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetDependentObjectsResponse
     */
    class GetDependentObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $dependentObjectType;

        /**
         * @var string
         *
         */
        public $superiorObjectGuid;

        /**
         * @var string
         *
         */
        public $superiorObjectType;

        /**
         * @var array
         *
         */
        public $fields;

        /**
         * @var string
         *
         */
        public $teamFilter;

        /**
         * @var string
         *
         */
        public $searchText;

        /**
         * @var array
         *
         */
        public $orderByColumns;

    }

}
