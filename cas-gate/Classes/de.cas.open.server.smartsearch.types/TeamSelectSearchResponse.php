<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the team select search result.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: TeamSelectSearchRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see TeamSelectSearchRequest
     */
    class TeamSelectSearchResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *Sets/Gets the right owners that match the query term.
         */
        public $Results;

        /**
         * @var unknown
         *The total number of hits.
         */
        public $NumHits;

        /**
         * @var unknown
         *The number of result pages.
         */
        public $NumPages;

    }

}
