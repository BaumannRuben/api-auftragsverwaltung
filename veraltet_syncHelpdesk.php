<pre>
<?php
include 'cas-gate/CASGate.php';

// $anzTickets = 0;
// $anzNew = 0;
// $anzUpdate = 0;
// $anzVk = 0;
// $anzNotVk = 0;
// $nichtKNR = array();

$datei = fopen("logs\\tasks\\aufgaben_" . date("Y-m-d-H-i-s") . ".txt", "w");

// $con = new mysqli('192.168.40.13', 'root', 'root', 'hochwarth-zeit');
$con = new mysqli('localhost', 'root', '', 'hh_zeit_test');
$con->set_charset('utf8');
if ($con->connect_errno) {
    fwrite($datei, "Verbindung fehlgeschlagen!\r\n");
    die("Verbindung fehlgeschlagen: " . $con->connect_error);
} else {
    fwrite($datei, "Verbindung erfolgreich!\r\n");
    fwrite($datei, "Beginn: ".date("Y-m-d H:i:s")."\r\n");
    echo "Verbindung erfolgreich hergestellt!<br><br>";
}


//Lade Tickets aus CAS
$option = array();
$option['fields'] = array('GGUID', 'KEYWORD', 'START_DT', 'END_DT', 'ITDSECTION', 'GWSSTATUS', 'GWSPRIORITY', 'GWSTICKETTYPE', 'ITDPROBLEM', 'ITDSOLUTION', 'NOTES2', 'E_AV_NUMMER', 'E_AV_LASTSYNC', 'UPDATETIMESTAMP');
$option['conditions'] = array('AND' =>
    array('TICKETGROUPNAME' => 'HELPDESK',
        'GWSTYPE' => 'Support',
        'GWSTICKETORIGIN' => 'Web',
        'GWSSTATUS !=' => 'abgeschlossen'
    )
);
$tickets = HochwarthIT_CASGate::getTaskDao()->find('all', $option);
// print_r($tickets);

//Synchronisierung Tickets aus CAS mit Status != abgeschlossen, unabh�ngig von �nderungen in der AV
foreach ($tickets as $ticket) {
    syncTicket($ticket, $con);
    downloadDocuments($ticket);
}

//Lade Tickets aus AV
$avTickets = $con->query("SELECT CASID, updatetimestamp, lastsync FROM tblaufgaben WHERE lastsync IS NOT NULL AND updatetimestamp > lastsync;");
$avTickets = $avTickets->fetch_all(MYSQLI_ASSOC);

$relevantAVTickets = array();
//Pr�fung Status in CAS
foreach ($avTickets as $avTicket) {
    $option = array();
    $option['fields'] = array('GGUID', 'KEYWORD', 'START_DT', 'END_DT', 'ITDSECTION', 'GWSSTATUS', 'GWSPRIORITY', 'GWSTICKETTYPE', 'ITDPROBLEM', 'ITDSOLUTION', 'NOTES2', 'E_AV_NUMMER', 'E_AV_LASTSYNC', 'UPDATETIMESTAMP');
    $option['conditions'] = array('GGUID' => "0x".$avTicket['CASID']);
    $tempTicket = HochwarthIT_CASGate::getTaskDao()->find('first', $option);
    if ($tempTicket['GWSSTATUS']['value'] == 'abgeschlossen') {
        array_push($relevantAVTickets, $tempTicket);
    }
}

//Synchronisierung Tickets aus CAS mit Status == abgeschlossen, welche sich zwischenzeitlich in der AV ge�ndert haben
foreach ($relevantAVTickets as $relevantAVTicket) {
    syncTicket($relevantAVTicket, $con);
}


//Hauptfunktion
function syncTicket($ticket, $con) {
    $tage = array("So","Mo","Di","Mi","Do","Fr","Sa");
    $kunde = HochwarthIT_CASGate::getTaskDao()->getLinkedKunde($ticket['GGUID']['value']);
    $ticket['KUNDE_GGUID'] = array('value' => $kunde[0]['GGUID']['value']);
    $ticket['KUNDE_COMPNAME'] = array('value' => $kunde[0]['COMPNAME']['value']);
    $ticket['KUNDE_HOCHWARTH_KUNDENNUMMER'] = array('value' => $kunde[0]['HOCHWARTH_KUNDENNUMMER']['value']);
    
    $melder = HochwarthIT_CASGate::getTaskDao()->getLinkedMelder($ticket['GGUID']['value']);
    $ticket['MELDER_GGUID'] = array('value' => $melder[0]['GGUID']['value']);
    $ticket['MELDER_NAME'] = array('value' => $melder[0]['NAME']['value']);
    $ticket['MELDER_CHRISTIANNAME'] = array('value' => $melder[0]['CHRISTIANNAME']['value']);
    $ticket['MELDER_ADDRESSTERM'] = array('value' => $melder[0]['ADDRESSTERM']['value']);
    $ticket['MELDER_PHONEFIELDSTR4'] = array('value' => $melder[0]['PHONEFIELDSTR4']['value']);
    
    switch($ticket['ITDSECTION']['value']) {
        case 'CRM':
            $ursprung_ma = 40;
            $gesellschaft = 1;
            
            break;
        case 'IT':
            $ursprung_ma = 0;
            $gesellschaft = 1;
            break;
        case 'ECOM':
            $ursprung_ma = 34;
            $gesellschaft = 1;
            break;
        default:
            $ursprung_ma = 0;
            $gesellschaft = 1;
            break;
    }
    if ($ticket['GWSPRIORITY']['value'] == 'Hoch')
        $offset = "+4 hours";
    else 
        $offset = "+1 day";
    
    //     print_r($ticket);
    
    /* Wenn Ticket keine AV_NUMMER besitzt, existiert es in der AV noch nicht => Neuanlage
     * Nach der Neuanlage werden die Felder in CAS gesetzt
     */
    if (empty($ticket['E_AV_NUMMER']['value'])) {
        $sql_afg = "INSERT INTO tblaufgaben (Zeitpunkt, Ursprung, KundeNr, Gesellschaft, Abteilung, MitarbeiterNr, Bezeichnung, Beschreibung, Kundeninfos, Prio, Status, ReaktionBis, readout, CASID, lastsync) ".
            "VALUES (".
            "'".date('Y-m-d\TH:i:s', strtotime($ticket['START_DT']['value']))."', ".
            "'".$ursprung_ma."', ".
            "'".$ticket['KUNDE_HOCHWARTH_KUNDENNUMMER']['value']."', ".
            "'".$gesellschaft."', ".
            "'".$ticket['ITDSECTION']['value']."', ".
            "'".$ursprung_ma."', ".
            "'". $ticket['KEYWORD']['value']."', ".
            "'".$ticket['ITDPROBLEM']['value']." AP: ".$ticket['MELDER_NAME']['value']."', ".
            "'".$ticket['NOTES2']['value']."', ".
            "'".$ticket['GWSPRIORITY']['value']."', ".
            "'".$ticket['GWSSTATUS']['value']."', ".
            "'".date('Y-m-d\TH:i:s', strtotime($offset))."', ".
            "'1', ".
            "'".$ticket['GGUID']['value']."', ".
            "'".date('Y-m-d\TH:i:s')."' ".
            ");";
//         echo $sql_afg."<br>";die();
        $con->query($sql_afg);
        
        
        $ticketAV = $con->query("SELECT AufgabeNr, CASID FROM tblaufgaben WHERE CASID = '".$ticket['GGUID']['value']."';");
        $ticketAV = $ticketAV->fetch_all(MYSQLI_ASSOC);
        $tempTicket = HochwarthIT_CASGate::getTaskDao()->load($ticket['GGUID']['value']);
        $tempTicket->setValue($ticketAV[0]['AufgabeNr'], 'E_AV_NUMMER', 'INT');
        $tempTicket->setValue($ticket['KUNDE_HOCHWARTH_KUNDENNUMMER']['value'], 'E_AV_KNDNR', 'INT'); 
        $tempTicket->setValue(date('Y-m-d\TH:i:s', strtotime('+1 second')), 'E_AV_LASTSYNC', 'DATETIME');
        $tempTicket = HochwarthIT_CASGate::getTaskDao()->save($tempTicket);
    }
    /* Wenn das Ticket bereits eine AV_NUMMER besitzt, existiert es in der AV => Aktualisierung
     * Fallunterscheidungen
     * 1. CAS-UPDATETIMESTAMP > CAS-LASTSYNC && AV-UPDATETIMESTAMP <= CAS-LAST => Aktualisierungen in AV speichern
     * 2.
     */
    else {
        //Zugeh�riges Ticket aus AV laden
        $ticketAV = $con->query("SELECT * FROM tblaufgaben WHERE CASID = '".$ticket['GGUID']['value']."';");
        $ticketAV = $ticketAV->fetch_all(MYSQLI_ASSOC);
        
        $casUpdate = FALSE;
        $avUpdate = FALSE;
        
        if($ticket['UPDATETIMESTAMP']['value'] > $ticket['E_AV_LASTSYNC']['value']) {
            //Neuerungen in CAS vorhanden (Notizen, Priorit�t)
            $casUpdate = TRUE;
        }
        if($ticketAV[0]['updatetimestamp'] > $ticketAV[0]['lastsync']) {
            //Neuerungen in AV vorhanden (Priorit�t, L�sung)
            $avUpdate = TRUE;
        }
        
        //Zugeh�rige Zeiten holen
        $ticketAVStunden = $con->query("SELECT s.StundenNr, s.AnlageDatum, s.Beschreibung, s.Anzahl, m.Vorname, m.Name FROM tblstunden s, tblmitarbeiter m WHERE s.MitarbeiterNr = m.MitarbeiterNr AND s.AufgabeNr = ".$ticketAV[0]['AufgabeNr']." AND s.HelpdeskOnline = 1 ORDER BY s.AnlageDatum DESC;");
        $ticketAVStunden = $ticketAVStunden->fetch_all(MYSQLI_ASSOC);
        
        $timeEntrys = array();
        foreach ($ticketAVStunden as $std){
            array_push($timeEntrys, "----- ". $tage[date("w", strtotime($std['AnlageDatum']))].", ".date('d.m.Y H:i', strtotime($std['AnlageDatum']))." (".$std['Vorname']." ".$std['Name'].") -----\n".$std['Beschreibung']."\n".$std['Anzahl']." Std.\n\r");
            $sql_update = "UPDATE tblstunden SET HelpdeskOnline = 2 WHERE StundenNr = '".$std['StundenNr']."';";
            $con->query($sql_update);
        }
        $times = implode("\n", $timeEntrys);
        //Zeiten vorhanden, dann vor Notizen einf�gen
        $notes2 = $ticket['NOTES2']['value'];
        if(!empty($times)) {
            $notes2 = $times."\n".$notes2;
        }
                
        echo "CAS $casUpdate AV $avUpdate<br>";
        //Fallunterscheidung
        if ($casUpdate && !$avUpdate) {
            //�nderungen von CAS in AV speichern
            //Abschneiden der Zeiten, nur Notizen in Kundeninfos �bertragen
                              
            $con->query("UPDATE tblaufgaben SET Prio = '".$ticket['GWSPRIORITY']['value']."', Status = '".$ticket['GWSSTATUS']['value']."', Kundeninfos = '".$notes2."', lastsync = '".date('Y-m-d\TH:i:s')."'  WHERE CASID = '".$ticket['GGUID']['value']."';");
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->load($ticket['GGUID']['value']);
            $tempTicket->setValue($notes2, 'NOTES2', 'STRING');
            $tempTicket->setValue(date('Y-m-d\TH:i:s', strtotime('+1 second')), 'E_AV_LASTSYNC', 'DATETIME');
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->save($tempTicket);
        }
        else if (!$casUpdate && $avUpdate) {
            //�nderungen von AV in CAS speichern
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->load($ticket['GGUID']['value']);
            $tempTicket->setValue($ticketAV[0]['Prio'], 'GWSPRIORITY', 'STRING');
            $tempTicket->setValue($ticketAV[0]['Status'], 'GWSSTATUS', 'STRING');
            $tempTicket->setValue($ticketAV[0]['Loesung'], 'ITDSOLUTION', 'STRING');
            $tempTicket->setValue($notes2, 'NOTES2', 'STRING');
            $tempTicket->setValue(date('Y-m-d\TH:i:s', strtotime('+1 second')), 'E_AV_LASTSYNC', 'DATETIME');
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->save($tempTicket);
            
            $con->query("UPDATE tblaufgaben SET lastsync = '".date('Y-m-d\TH:i:s')."'  WHERE CASID = '".$ticket['GGUID']['value']."';");
        }
        else if ($casUpdate && $avUpdate) {
            //�nderungen sowohl in AV als auch in CAS
            
            //Priorit�t, Status => CAS f�hrend
            $con->query("UPDATE tblaufgaben SET Prio = '".$ticket['GWSPRIORITY']['value']."', Status = '".$ticket['GWSSTATUS']['value']."', Kundeninfos = '".$notes2."', lastsync = '".date('Y-m-d\TH:i:s')."'  WHERE CASID = '".$ticket['GGUID']['value']."';");
            
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->load($ticket['GGUID']['value']);
            $tempTicket->setValue($ticketAV[0]['Loesung'], 'ITDSOLUTION', 'STRING');
            $tempTicket->setValue($notes2, 'NOTES2', 'STRING');
            $tempTicket->setValue(date('Y-m-d\TH:i:s', strtotime('+1 second')), 'E_AV_LASTSYNC', 'DATETIME');
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->save($tempTicket);
            
        }
        else if (!$casUpdate && !$avUpdate) {
            //�nderungen weder in AV noch in CAS
            
            $con->query("UPDATE tblaufgaben SET Prio = '".$ticket['GWSPRIORITY']['value']."', Status = '".$ticket['GWSSTATUS']['value']."', Kundeninfos = '".$notes2."', lastsync = '".date('Y-m-d\TH:i:s')."'  WHERE CASID = '".$ticket['GGUID']['value']."';");
            
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->load($ticket['GGUID']['value']);
            $tempTicket->setValue($notes2, 'NOTES2', 'STRING');
            $tempTicket->setValue(date('Y-m-d\TH:i:s', strtotime('+1 second')), 'E_AV_LASTSYNC', 'DATETIME');
            $tempTicket = HochwarthIT_CASGate::getTaskDao()->save($tempTicket);
        }
    }
}


function downloadDocuments($ticket) {
    $documents = HochwarthIT_CASGate::getTaskDao()->getNewLinkedDocuments($ticket['GGUID']['value']);
    foreach ($documents as $document) {
        $result = HochwarthIT_CASGate::getDocumentDao()->downloadDocument($document['GGUID']['value']);
        $filetype = $result->fileType;
        $filename = $document['KEYWORD']['value'];
        $data = $result->documentContent;
        if (!file_exists("C:\\".$ticket['E_AV_NUMMER']['value'])) {
            mkdir("C:\\".$ticket['E_AV_NUMMER']['value'], 0777, true);
        }
        $datei = fopen("C:\\".$ticket['E_AV_NUMMER']['value']."\\".$filename.".".$filetype,"w");
        fwrite($datei, $data);
        fclose($datei);
        
        $tempdocument = HochwarthIT_CASGate::getDocumentDao()->load($document['GGUID']['value']);
        $tempdocument->setValue(TRUE, 'E_DOWNLOAD', 'BOOLEAN');
        $tempdocument = HochwarthIT_CASGate::getDocumentDao()->save($tempdocument);
    }
}
?>
</pre>