<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *                \de\cas\open\server\api\types\RequestObject: Gets the content of a cluster-popup, formatted using a template.
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class GetGeoDataPopupDetailsFromTemplateRequest extends \de\cas\gw\server\geodata\types\GetGeoDataPopupDetailsRequest {

        /**
         * @var string
         *
         *                                        Sets/Returns the TemplateName for the geodata request.
         */
        public $TemplateName;

    }

}
