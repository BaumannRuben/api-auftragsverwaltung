<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the html notes for the dataobject with the
     *        given GGUID from the given fieldname. Corresponding \de\cas\open\server\api\types\ResponseObject: GetFormattedNotesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFormattedNotesResponse
     */
    class GetFormattedNotesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The object type of the dataobject.
         */
        public $ObjectType;

        /**
         * @var string
         *
         *                    The GGUID of the dataobject.
         */
        public $GGUID;

        /**
         * @var string
         *
         *                    The fieldname of the dataobject.
         */
        public $FieldName;

        /**
         * @var int
         *
         *                    Determines what should be filtered(IFRAME, onClick, IMG
         *                    src="...", ...). Please see \de\cas\eim\api\constants\HtmlFilterConstants.
         *	@see \de\cas\eim\api\constants\HtmlFilterConstants
         */
        public $FilterValue;

    }

}
