<?php
// This file has been automatically generated.

namespace de\cas\gw\server\addresses\types {

    /**
     * @package de\cas\gw\server\addresses
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject of the business operation that exports
     *						address records with their linked personal notes.
     *						Corresponding \de\cas\open\server\api\types\ResponseObject: ExportAddressesWithPersonalNotesAsCSVResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ExportAddressesWithPersonalNotesAsCSVResponse
     */
    class ExportAddressesWithPersonalNotesAsCSVRequest extends \de\cas\open\server\csvexport\types\ExportAsCSVRequest {

    }

}
