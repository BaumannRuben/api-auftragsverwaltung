<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Saves all dossier settings for the supplied DataObjectType and the current user.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: SaveDossierSettingsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveDossierSettingsResponse
     */
    class SaveDossierSettingsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $DataObjectType;

        /**
         * @var \de\cas\gw\server\core\types\DossierSettings
         *
         */
        public $DossierSettings;

    }

}
