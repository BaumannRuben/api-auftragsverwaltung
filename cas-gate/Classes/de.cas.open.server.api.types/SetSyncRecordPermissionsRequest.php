<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Request Object to update participants of a record
     *        when ExchangeSync is active. You can not add or delete participants,
     *        only modify existing participants.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        SetSyncRecordPermissionsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetSyncRecordPermissionsResponse
     */
    class SetSyncRecordPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Gets/Sets the tablename of the record.
         */
        public $tableName;

        /**
         * @var string
         *
         *                    Gets/Sets the Guid of the record.
         */
        public $tableGuid;

        /**
         * @var \de\cas\open\server\api\types\SyncRecordPermissions
         *
         *                    Gets/sets the permissions of the synchronized record.
         */
        public $SyncRecordPermissions;

    }

}
