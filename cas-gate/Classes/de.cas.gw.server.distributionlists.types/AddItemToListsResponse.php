<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Adds an address to multiple distribution lists.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: AddItemToListsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AddItemToListsRequest
     */
    class AddItemToListsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Indicates if the operation for the address at the specific index was successfull or not.
         */
        public $result;

    }

}
