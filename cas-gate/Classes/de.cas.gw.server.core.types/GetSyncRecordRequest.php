<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     */
    class GetSyncRecordRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $DataObjectType;

        /**
         * @var string
         *
         */
        public $TableGUID;

    }

}
