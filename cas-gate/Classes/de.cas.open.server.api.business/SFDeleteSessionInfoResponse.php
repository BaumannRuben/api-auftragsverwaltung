<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/>
     *       		Deletes a session information entry with the same sessionId value. No reponse values.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: SFDeleteSessionInfoRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SFDeleteSessionInfoRequest
     */
    class SFDeleteSessionInfoResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
