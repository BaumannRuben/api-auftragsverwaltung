<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        Formats the phone number according to the given country code.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: FormatPhoneNumberRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see FormatPhoneNumberRequest
     */
    class FormatPhoneNumberResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The formatted phone number.
         */
        public $phoneNumber;

    }

}
