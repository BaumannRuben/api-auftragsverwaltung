<?php
// This file has been automatically generated.

namespace de\cas\open\server\csvexport\types {

    /**
     * @package de\cas\open\server\csvexport
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject of the business operation that exports
     *				the records with the given gguids, or matching the given where clause and type in csv format.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: ExportAsCSVRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ExportAsCSVRequest
     */
    class ExportAsCSVResponse extends \de\cas\open\server\csvexport\types\AbstractExportAsCSVResponse {

    }

}
