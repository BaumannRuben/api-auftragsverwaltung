<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the \de\cas\open\server\business\BusinessOperation that deletes users, groups or resources by oids.
     *				The oids can contain mixed oids from different permission owner types (USERs, RESOURCEs or
     * 				GROUPs). It deletes not only the records of the permission owners but also
     *  				related records ( permissions, groupmemberships...)
     *  				<br>
     * 				The data object permissions of the permission owners will also be deleted or
     * 				transfered. That means if the permissions of an private data object belong only to
     *  				the permission owners to delete, the data object and its permissions will be
     *  				deleted too. But if the permissions of this data object belong also to an
     *  				other user which will not be deleted or the forein edit permissions are not
     *  				private the permissions will be transfered to an other user.<br />
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: DeletePermissionsOwnersResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeletePermissionsOwnersResponse
     */
    class DeletePermissionsOwnersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *									The oids of the permission owners to delete. The oids in the
         *	 								list can belong to users, groups and resources. The list can
         *	 								contain mixed oids from different permission owner types.
         */
        public $permissionsOwnersOids;

        /**
         * @var int
         *
         *									The oid of an user to which the data object permissions of the
         *	             					deleted users are transfered.
         *	            					That means if the permissions of a data object belong not only
         *	            					to the users which will be deleted or the forein edit permissions are not
         *  									private the data object permissions will be transfered to this user.
         */
        public $transferUserOid;

    }

}
