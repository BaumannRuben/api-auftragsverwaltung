<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request Object to receive all groups containing the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetGroupsContainingUserResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGroupsContainingUserResponse
     */
    class GetGroupsContainingUserRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The GGUID of the requested user.
         */
        public $gguid;

    }

}
