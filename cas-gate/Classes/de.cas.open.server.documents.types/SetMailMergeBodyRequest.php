<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: This operation set the mhtml content of a mailmerge document
     *        Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: SetMailMergeBodyResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetMailMergeBodyResponse
     */
    class SetMailMergeBodyRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *                    GGUID of the document record
         */
        public $mhtmlContent;

        /**
         * @var string
         *
         *                    GGUID of the document record
         */
        public $docGguid;

    }

}
