<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\RequestObject: Checks the given password of the current user.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CheckPasswordResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckPasswordResponse
     */
    class CheckPasswordRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The password to check.
         */
        public $password;

    }

}
