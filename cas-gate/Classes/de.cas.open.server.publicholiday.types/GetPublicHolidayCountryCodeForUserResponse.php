<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves the country code for
     *        public holidays configured for the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetPublicHolidayCountryCodeForUserRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPublicHolidayCountryCodeForUserRequest
     */
    class GetPublicHolidayCountryCodeForUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var int
         *
         *                    countryCode of the user
         */
        public $countryCode;

    }

}
