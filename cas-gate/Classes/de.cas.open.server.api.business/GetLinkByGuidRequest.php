<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.RequestObject}: Retrieves a link identified by its GGUID. Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetLinkByGuidResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetLinkByGuidResponse
     */
    class GetLinkByGuidRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Gets/Sets the GGUID of the link to be retrieved.
         */
        public $GGUID;

    }

}
