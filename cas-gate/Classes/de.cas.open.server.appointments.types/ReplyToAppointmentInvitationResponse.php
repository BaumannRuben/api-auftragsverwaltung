<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject:
     *        It's the base type for all business operation
     *        response objects. Corresponding RequestObject: ReplyToAppointmentInvitationRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ReplyToAppointmentInvitationRequest
     */
    class ReplyToAppointmentInvitationResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
