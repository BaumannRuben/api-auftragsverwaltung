<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Does a team select search.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: TeamSelectSearchResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see TeamSelectSearchResponse
     */
    class TeamSelectSearchRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Sets/Gets the search term
         */
        public $Term;

        /**
         * @var string
         *Data type of the source record
         */
        public $ObjectType;

        /**
         * @var array
         *Right owner types to search
         */
        public $RightOwnerTypes;

        /**
         * @var boolean
         *Include free/busy information for returned right owners
         */
        public $IncludeFreeBusyInformation;

        /**
         * @var unknown
         *Start time for free/busy check
         */
        public $FreeBusyStart;

        /**
         * @var unknown
         *End time for free/busy check
         */
        public $FreeBusyEnd;

        /**
         * @var array
         *Domain IDs to search for right owners (empty: all domains)
         */
        public $DomainIds;

        /**
         * @var string
         *Keyword of the source record. It is used to adapt the ranking using records with the same data type and a similar keyword
         */
        public $SourceRecordKeyword;

        /**
         * @var array
         *Right owner ids to exclude from the result list
         */
        public $ExcludeRightOwnerIds;

        /**
         * @var array
         *Right owner GUIDs to exclude from the result list
         */
        public $ExcludeGuids;

        /**
         * @var unknown
         *Specifies the result page to retrieve
         */
        public $Page;

        /**
         * @var unknown
         *Specifies the number of results in a page
         */
        public $PageSize;

    }

}
