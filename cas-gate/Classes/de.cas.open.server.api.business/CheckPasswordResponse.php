<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\ResponseObject
     *        Returns TRUE if the password is valid for the current user. Otherwise FALSE is returned.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CheckPasswordRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckPasswordRequest
     */
    class CheckPasswordResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Returns TRUE if the password is valid for the current user. Otherwise FALSE is returned.
         */
        public $valid;

    }

}
