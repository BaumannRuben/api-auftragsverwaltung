<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Deletes an address from multiple distribution lists.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: DeleteItemFromListsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteItemFromListsResponse
     */
    class DeleteItemFromListsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										GGUIDs of the distribution lists
         */
        public $DistributionListGGUIDs;

        /**
         * @var string
         *
         *										GGUID of the address record
         */
        public $AddressGGUID;

    }

}
