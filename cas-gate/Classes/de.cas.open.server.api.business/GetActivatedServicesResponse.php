<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the services that are activated on the application server.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetActivatedServicesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetActivatedServicesRequest
     */
    class GetActivatedServicesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Contains the services that are activated on the application server.
         */
        public $activatedServices;

    }

}
