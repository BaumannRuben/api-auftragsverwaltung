<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject
     *        of the business operation that calculates an IBAN from the given
     *        bank data.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        CalculateIbanResponse.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CalculateIbanResponse
     */
    class CalculateIbanRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\addressesvalidation\types\BankAccountData
         *
         *                    \de\cas\open\server\addresses\validation\types\BankAccountData
         *                    from which the IBAN should be calculated.
         *	@see \de\cas\open\server\addresses\validation\types\BankAccountData
         */
        public $bankAccountData;

        /**
         * @var string
         *
         *                    The ISO code of the country. Mandatory.
         */
        public $countryCode;

    }

}
