<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> See request object.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: LoginCheckRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see LoginCheckRequest
     */
    class LoginCheckResponse extends \de\cas\open\server\api\business\EmptyResponse {

    }

}
