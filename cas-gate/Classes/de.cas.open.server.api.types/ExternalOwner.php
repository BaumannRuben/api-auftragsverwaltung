<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Represents an external owner of a record that is managed by the sync service.
     */
    class ExternalOwner {

        /**
         * @var string
         *
         *                Gets/Sets the gguid of the external owner. Depending on the
         *                ExternalOwnerType, this field will contain the gguid
         *                of the user/resource for users/resources,
         *                the guid of the address-record in case of an address
         *                or null in case of an e-mail-address.
         */
        public $Guid;

        /**
         * @var string
         *
         */
        public $EmailAddress;

        /**
         * @var string
         *
         */
        public $DisplayName;

        /**
         * @var string
         *
         */
        public $InvitationStatus;

        /**
         * @var boolean
         *
         */
        public $IsRequired;

        /**
         * @var string
         *
         */
        public $Type;

    }

}
