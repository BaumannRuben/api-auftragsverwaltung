<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     */
    class GetSyncRecordResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    Saved dataobject.
         */
        public $DataObject;

        /**
         * @var \de\cas\open\server\api\types\SyncRecordPermissions
         *
         *                    The permissions that were saved for the DataObject.
         *                    If ExchangeSync or InvitationManagement isn't active this object is not set.
         */
        public $Permissions;

    }

}
