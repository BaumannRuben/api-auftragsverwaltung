<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Responseobject for the businessoperation
     *        GetEmailSettings. It contains the loaded emailSettings and
     *        associated emailArchiveSettings.
     */
    class GetEmailSettingsByUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Sets/Returns the resultobject containing the
         *                    loaded emailSettings and associated emailArchiveSettings.
         */
        public $emailSettings;

    }

}
