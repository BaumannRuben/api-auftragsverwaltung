<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Requestobject for the businessoperation
     *        GetEmailSettingsByUser, which returns the EmailSettings and
     *        the associated EmailArchiveSettings by a user.
     *	@see EmailSettings
     *	@see EmailArchiveSettings
     */
    class GetEmailSettingsByUserRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *                    Sets/Returns the userOID to which the
         *                    emailSettings will be returned.
         */
        public $userOID;

    }

}
