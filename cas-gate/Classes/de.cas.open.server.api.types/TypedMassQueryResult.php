<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *		    This is a container object for a MassQueryResult object and a type.
     *		    All fields of this result are of one object type.
     */
    class TypedMassQueryResult {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *This field contains the data.
         */
        public $data;

        /**
         * @var string
         *
         *								This is the data object type of all fields in the
         *								MassQueryResult.
         */
        public $objectType;

    }

}
