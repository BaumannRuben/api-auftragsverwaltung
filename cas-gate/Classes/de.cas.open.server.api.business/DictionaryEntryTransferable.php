<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Transports a dictionary key and the translations.
     *        Can be converted from and to a \de\cas\open\server\dataaccess\DictionaryEntry.
     *	@see \de\cas\open\server\dataaccess\DictionaryEntry
     */
    class DictionaryEntryTransferable {

        /**
         * @var string
         *
         *                The key in the dictionary (usually <code>TABLE.COLUMN</code>).
         */
        public $dictionaryKey;

        /**
         * @var array
         *
         *                The languages sent in {@link #displayNames} and {@link #displayNamesShort}.
         */
        public $languages;

        /**
         * @var array
         *
         *                The displaynames (translations), positions in the list as in {@link #languages}.
         */
        public $displayNames;

        /**
         * @var array
         *
         *                The short displaynames (translations) to be displayed in lists, positions in the list as in {@link #languages}.
         */
        public $shortDisplayNames;

    }

}
