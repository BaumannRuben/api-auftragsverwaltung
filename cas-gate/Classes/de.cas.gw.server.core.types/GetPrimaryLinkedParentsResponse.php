<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the list of primary linked parent objects
     *        as a list of ListItemOverview.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetPrimaryLinkedParentsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ListItemOverview
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPrimaryLinkedParentsRequest
     */
    class GetPrimaryLinkedParentsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $primaryLinkedParents;

    }

}
