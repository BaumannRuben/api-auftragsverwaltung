<?php
// This file has been automatically generated.

namespace de\cas\open\server\systemnews\types {

    /**
     * @package de\cas\open\server\systemnews
     * @subpackage types
     *This is a container for all relevant system news information.
     */
    class SystemNews {

        /**
         * @var string
         *Sets/Returns the GUID.
         */
        public $guid;

        /**
         * @var unknown
         *Sets/Returns the start date of the valid period.
         */
        public $validFrom;

        /**
         * @var unknown
         *Sets/Returns the end date of the valid period.
         */
        public $validTo;

        /**
         * @var string
         *Sets/Returns the user that inserted the news.
         */
        public $insertUser;

        /**
         * @var string
         *Sets/Returns the user that last updated the news.
         */
        public $updateUser;

        /**
         * @var unknown
         *Sets/Returns the timestamp the news was created on.
         */
        public $InsertTimestamp;

        /**
         * @var unknown
         *Sets/Returns the timestamp the news was last modified.
         */
        public $UpdateTimestamp;

        /**
         * @var string
         *Sets/Returns the keyword.
         */
        public $keyword;

        /**
         * @var string
         *Sets/Returns the message.
         */
        public $message;

        /**
         * @var string
         *Sets/Returns the language of the message.
         */
        public $language;

        /**
         * @var int
         *Sets/Returns the priority.
         */
        public $priority;

        /**
         * @var string
         *Sets/Returns the client prefix.
         */
        public $clientPrefix;

        /**
         * @var string
         *Sets/Returns the customer group.
         */
        public $customerGroup;

    }

}
