<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the html notes for the dataobject with the
     *        given GGUID from the given fieldname. Corresponding \de\cas\open\server\api\types\RequestObject: GetFormattedNotesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFormattedNotesRequest
     */
    class GetFormattedNotesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *Sets/Returns the file content
         */
        public $fileContent;

        /**
         * @var string
         *
         *                    Sets/Returns the file extension
         */
        public $fileExtension;

    }

}
