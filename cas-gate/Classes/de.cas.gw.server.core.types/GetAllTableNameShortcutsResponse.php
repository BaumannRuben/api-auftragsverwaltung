<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves all object types and their shortcuts.
     *        This shortcuts are relevant in linking concepts.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetAllTableNameShortcutsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllTableNameShortcutsRequest
     */
    class GetAllTableNameShortcutsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $TableNameShortcuts;

    }

}
