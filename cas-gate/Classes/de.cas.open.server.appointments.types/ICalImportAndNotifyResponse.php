<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject:
     *        It's the base type for all business operation
     *        response objects. Corresponding RequestObject: \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     */
    class ICalImportAndNotifyResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    Returns the GGUID of the created Appointment
         */
        public $GGUID;

    }

}
