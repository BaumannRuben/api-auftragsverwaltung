<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject for re-calculating fields of a opportunity position
     *        when a field changes.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CalculateOpportunityPositionValuesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CalculateOpportunityPositionValuesRequest
     */
    class CalculateOpportunityPositionValuesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Sets/Returns the updated opportunity position.
         */
        public $opportunityPositions;

    }

}
