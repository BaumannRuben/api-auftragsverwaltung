<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request Object to receive a detailed group object.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetGroupResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGroupResponse
     */
    class GetGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										The id of the requested group. This property is
         *										preferred over the GGUID.
         */
        public $id;

        /**
         * @var string
         *
         *										The GGUID of the requested group.
         */
        public $gguid;

    }

}
