<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that gets system-properties that
     *				belong to a propertyGroup.
     */
    class GetSystemPropertiesByGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\EIMPropertiesTransferable
         *
         */
        public $properties;

    }

}
