<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the Operation that returns all supported currencies. Corresponding \de\cas\open\server\api\types\ResponseObject: GetSupportedCurrenciesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSupportedCurrenciesResponse
     */
    class GetSupportedCurrenciesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
