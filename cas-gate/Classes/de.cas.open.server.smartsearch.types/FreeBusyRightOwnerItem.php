<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *Class representing a single right owner item and the list of associated data objects
     */
    class FreeBusyRightOwnerItem {

        /**
         * @var string
         *GGUID of the right owner
         */
        public $GGUID;

        /**
         * @var boolean
         *Specifies if the logged in user has access to this right owner
         */
        public $HasAccess;

        /**
         * @var array
         *List of data objects
         */
        public $DataObjects;

    }

}
