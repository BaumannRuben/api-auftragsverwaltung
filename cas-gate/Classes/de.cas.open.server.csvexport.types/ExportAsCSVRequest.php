<?php
// This file has been automatically generated.

namespace de\cas\open\server\csvexport\types {

    /**
     * @package de\cas\open\server\csvexport
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject of the business operation that exports
     *						the records with the given gguids, or matching the given where clause and type in csv format.
     *		      			Only one of where clause and gguids request parameters has to be filled. If no guids and no where clause
     *		      			is set, then all objects with the given type are exported.
     *						Corresponding \de\cas\open\server\api\types\ResponseObject: ExportAsCSVResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ExportAsCSVResponse
     */
    class ExportAsCSVRequest extends \de\cas\open\server\csvexport\types\AbstractExportAsCSVRequest {

        /**
         * @var array
         *
         *                    The list of gguids of addresses to export in csv
         *                    format.
         */
        public $gguids;

        /**
         * @var string
         *
         *                    The condition the addresses to be exported have
         *                    to fulfill. If a source table is provided,
         *                    it must be the data object type (the reason is,
         *                    that the RAP client uses this prefix anyway).
         */
        public $whereClause;

        /**
         * @var string
         *
         *                    The order by clause that should be used to sort the
         *                    record results.
         */
        public $orderByClause;

        /**
         * @var string
         *The type of object to export.
         */
        public $dataObjectType;

    }

}
