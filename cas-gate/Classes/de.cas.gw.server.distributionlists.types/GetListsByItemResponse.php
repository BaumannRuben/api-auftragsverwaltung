<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the distribution lists that
     *        contain a given address record.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetListsByItemRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetListsByItemRequest
     */
    class GetListsByItemResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										MassQueryResult with the following columns:<br/>
         *										GGUID: GGUID<br/>
         *										KEYWORD: Keyword of the distributor<br/>
         *										DESCRIPTION: Description of the distributor
         */
        public $result;

    }

}
