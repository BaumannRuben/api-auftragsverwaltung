<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Queries free/busy information for given right owners.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: FreeBusyQueryResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see FreeBusyQueryResponse
     */
    class FreeBusyQueryRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *Sets/Gets the list of right owner GUIDs to query free/busy information
         */
        public $RightOwnerGUIDs;

        /**
         * @var unknown
         *Start time of the query interval
         */
        public $StartTime;

        /**
         * @var unknown
         *End time of the query interval
         */
        public $EndTime;

        /**
         * @var array
         *Data objects to exclude from the query
         */
        public $ExcludeDataObjects;

        /**
         * @var array
         *Data object types to consider in the given query interval
         */
        public $DataObjectTypes;

    }

}
