<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Retrieves attachments for an eMail. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetEMailAttachmentsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetEMailAttachmentsRequest
     */
    class GetEMailAttachmentsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *Bytes of the files.
         */
        public $Files;

        /**
         * @var array
         *Filenames of the files.
         */
        public $Filenames;

    }

}
