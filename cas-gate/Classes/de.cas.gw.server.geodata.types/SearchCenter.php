<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *                Center of the area search. Street and City properties are empty if there are more than one address as search input.
     */
    class SearchCenter {

        /**
         * @var string
         *
         */
        public $Street;

        /**
         * @var string
         *
         */
        public $City;

        /**
         * @var double
         *
         */
        public $Latitude;

        /**
         * @var double
         *
         */
        public $Longitude;

    }

}
