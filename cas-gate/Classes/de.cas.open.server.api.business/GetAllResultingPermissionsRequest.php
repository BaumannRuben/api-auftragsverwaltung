<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Fetches all permissions (direct- and foreign-edit-permissions) that result from
     *				participants on a \de\cas\open\server\api\types\DataObject. Corresponding \de\cas\open\server\api\types\ResponseObject: GetAllResultingPermissionsResponse
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllResultingPermissionsResponse
     */
    class GetAllResultingPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *									The foreignEditPermissionRestriction that is set for the
         *									current DataObject.
         */
        public $foreignEditPermissionRestrictions;

        /**
         * @var array
         *
         *									The DataObjectPermissions that are set for the current
         *									DataObject.
         */
        public $dataObjectPermissions;

        /**
         * @var string
         *
         *									The DataObjectType of the current DataObject.
         */
        public $dataObjectType;

    }

}
