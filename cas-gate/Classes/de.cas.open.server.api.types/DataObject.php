<?php

namespace de\cas\open\server\api\types {

    use \de\cas\open\server\api\types\DataObjectTransferable;

    /**
     * A DataObject is a "generic" DataObject. CAS provides a set of the most common data types
     * (like appointment, address, ...) but also supports the creation of new data types. This is
     * why we use a "generic" object instead of an strongly typed object. The object type is
     * specified by a string property (instead of class names), which are listed in the
     * "DataObjectType" enumeration.
     *
     * @package de\cas\open\server\api
     * @subpackage types
     *
     * @see DataObjectTransferable
     */
    class DataObject {

        const GGUID = 'GGUID';
        const INSERTUSER = 'INSERTUSER';
        const UPDATEUSER = 'UPDATEUSER';
        const INSERTTIMESTAMP = 'INSERTTIMESTAMP';
        const UPDATETIMESTAMP = 'UPDATETIMESTAMP';
        const OWNERNAME = 'OWNERNAME';
        const OWNERGUID = 'OWNERGUID';
        const FOREIGNEDITPERMISSION = 'FOREIGNEDITPERMISSION';


        /**
         * @var array
         */
        private $fieldMap = array();

        /**
         * @var array
         */
        private $nullFields = array();

        /**
         * @var array
         */
        private $dirtyFields = array();

        /**
         * @var string
         */
        private $objectType;

        /**
         * @var string
         */
        private $client;

        /**
         * @var string
         */
        private $foreignEditPermissionRestriction = 'FOREIGNEDITPERMISSION_IN_EFFECT';

        /**
         * @var string
         */
        private $foreignEditPermissionRestrictionOld;

        /**
         * @var array
         */
        private $permissions = array();

        /**
         * @var array
         */
        private $tags = array();

        /**
         * @var unknown
         */
        private $descriptionVersion;

        /**
         * @var boolean
         */
        private $writable;

        /**
         * @var boolean
         */
        private $tagsDirty;

        /**
         * @var boolean
         */
        private $permissionsDirty;


        /**
         * @return string
         */
        public function getClient() {
            return $this->client;
        }

        /**
         * @param string $client
         * @return void
         */
        public function setClient($client) {
            $this->client = $client;
        }

        /**
         * @return string
         */
        public function getObjectType() {
            return $this->objectType;
        }

        /**
         * @param string $objectType
         * @return void
         */
        public function setObjectType($objectType) {
            $this->objectType = strtoupper($objectType);
        }

        /**
         * @return boolean
         */
        public function getWritable() {
            return $this->writable;
        }

        /**
         * @param boolean $writable
         * @return void
         */
        public function setWritable($writable) {
            $this->writable = $writable;
        }

        /**
         * @return unknown
         */
        public function getDescriptionVersion() {
            return $this->descriptionVersion;
        }

        /**
         * @param unknown $descriptionVersion
         * @return void
         */
        public function setDescriptionVersion($descriptionVersion) {
            $this->descriptionVersion = $descriptionVersion;
        }

        /**
         * @return array
         */
        public function getPermissions() {
            return $this->permissions;
        }

        /**
         * @return array
         */
        public function getTags() {
            return $this->tags;
        }

        /**
         * @param array $tags
         * @return void
         */
        public function setTags(array $tags) {
            $newTags = array_values($tags);
            $newTagsDirty = $this->tagsDirty || $this->tags != $newTags;
            $this->tags = $newTags;
            $this->tagsDirty = $newTagsDirty;
        }

        /**
         * @return array
         */
        public function getDirtyFields() {
            return $this->dirtyFields;
        }

        /**
         * @param array $dirtyFields
         * @return void
         */
        public function setDirtyFields(array $dirtyFields) {
            $this->dirtyFields = $dirtyFields;
        }

        /**
         * @return array
         */
        public function getNullFields() {
            return $this->nullFields;
        }

        /**
         * @param array $nullFields
         * @return void
         */
        public function setNullFields(array $nullFields) {
            $this->nullFields = $nullFields;
        }

        /**
         * @return boolean
         */
        public function getTagsDirty() {
            return $this->tagsDirty;
        }

        /**
         * @param boolean $tagsDirty
         * @return void
         */
        public function setTagsDirty($tagsDirty) {
            $this->tagsDirty = $tagsDirty;
        }

        /**
         * @return boolean
         */
        public function getPermissionsDirty() {
            return $this->permissionsDirty;
        }

        /**
         * @param boolean $permissionsDirty
         * @return void
         */
        public function setPermissionsDirty($permissionsDirty) {
            $this->permissionsDirty = $permissionsDirty;
        }

        /**
         * @return string
         */
        public function getForeignEditPermissionRestriction() {
            return $this->foreignEditPermissionRestriction;
        }

        /**
         * @param string $foreignEditPermissionRestriction
         * @return void
         */
        public function setForeignEditPermissionRestriction($foreignEditPermissionRestriction) {
            $this->foreignEditPermissionRestriction = $foreignEditPermissionRestriction;
        }


        /**
         * Creates a new DataObject.
         *
         * @param string $type
         * @param string $client
         * @param unknown $descriptionVersion
         */
        public function __construct($type, $client = NULL, $descriptionVersion = NULL) {
            $this->objectType = strtoupper($type);
            $this->client = $client;
            $this->descriptionVersion = $descriptionVersion;
            $this->foreignEditPermissionRestrictionOld = $this->foreignEditPermissionRestriction;
        }

        /**
         * Creates a DataObject from a DataObjectTransferable.
         *
         * @param DataObjectTransferable $transferable
         * @return DataObject
         */
        public static function fromDataObjectTransferable(DataObjectTransferable $transferable) {
            $dataObject = new static($transferable->objectType,
                                     $transferable->client,
                                     $transferable->descriptionVersion);

            // first copy null fields
            if ($transferable->nullFields !== NULL) {
                $dataObject->nullFields = array_values($transferable->nullFields);
            }
            $dataObject->copyFieldsToMap($transferable->fields);

            if ($transferable->permissions !== NULL) {
                $dataObject->permissions = array_values($transferable->permissions);
            }
            $dataObject->permissionsDirty = $transferable->permissionsDirty;

            if ($transferable->dirtyFields !== NULL) {
                $dataObject->dirtyFields = array_values($transferable->dirtyFields);
            }

            $dataObject->foreignEditPermissionRestriction = $transferable->foreignEditPermissionRestriction;
            $dataObject->foreignEditPermissionRestrictionOld = $dataObject->foreignEditPermissionRestriction;
            $dataObject->writable = $transferable->writable;

            if ($transferable->localizedTags !== NULL) {
                $dataObject->tags = array_values($transferable->localizedTags);
            }
            $dataObject->tagsDirty = $transferable->tagsDirty;

            return $dataObject;
        }

        /**
         * @param array $fieldList
         * @return void
         */
        private function copyFieldsToMap(array $fieldList = NULL) {
            if ($fieldList === NULL)
                return;
            foreach ($fieldList as $field) {
                $fieldName = $field->name;
                // if the field is in the map AND flagged as null, the map value is used
                $nullKey = array_search($fieldName, $this->nullFields, TRUE);
                if ($nullKey !== FALSE) {
                    unset($this->nullFields[$nullKey]);
                }
                $this->fieldMap[$fieldName] = $field;
            }
        }

        /**
         * Clones a DataObject with all fields.
         *
         * @return void
         */
        public function __clone() {
            // clone all fields
            foreach ($this->fieldMap as $fieldName => &$field) {
                $field = clone $field;
            }
        }

        /**
         * @param string $fieldName
         * @return mixed
         */
        public function getValue($fieldName) {
            $field = $this->getField($fieldName);
            if ($field === NULL) {
                return NULL;
            }
            if (property_exists($field, 'value')) {
                $value = $field->value;
            } elseif (property_exists($field, 'values')) {
                $value = $field->values;
                if (is_array($value))
                    $value = implode(', ', $value);
            }

            $type = $this->getPHPTypeForFieldType($field->fieldType);
            $this->setPHPTypeOfValue($value, $type);

            return $value;
        }

        /**
         * @param string $fieldName
         * @return Field
         */
        protected function getField($fieldName) {
            if ($this->isNullField($fieldName)) {
                return NULL;
            }
            $upperFieldName = strtoupper($fieldName);
            $field = $this->fieldMap[$upperFieldName];
            return $field;
        }

        /**
         * @param string $fieldName
         * @return boolean
         */
        public function isNullField($fieldName) {
            return in_array(strtoupper($fieldName), $this->nullFields, TRUE);
        }

        /**
         * @param mixed $value
         * @param string $fieldName
         * @param string $fieldType
         * @param boolean $forceSetValue explicitly set the field even if the value is empty
         * @return void
         */
        public function setValue($value, $fieldName, $fieldType = NULL, $forceSetValue = FALSE) {
            // set value (new field or replace old value)
            $newValue = NULL;
            if ($value !== NULL && $value !== '') {
                $newValue = $value;
            }
            $upperFieldName = strtoupper($fieldName);
            if ($this->isFieldDirty($upperFieldName, $newValue, $fieldType) || $forceSetValue) {
                if ($newValue === NULL && !$forceSetValue) {
                    $this->setFieldToNull($upperFieldName);
                } else {
                    // create field
                    if ($fieldType === NULL) {
                        if (!isset($this->fieldMap[$upperFieldName]))
                            throw new \Exception("Field '{$upperFieldName}' was not found in DataObject of type '{$this->objectType}'");
                        $fieldType = $this->fieldMap[$upperFieldName]->fieldType;
                    }
                    $fieldClass = $this->getFieldClass($fieldType);
                    if ($fieldClass === NULL)
                        throw new \InvalidArgumentException("No FieldTypeHandler found for FieldType: {$fieldType}");
                    if ($newValue === NULL)
                        $newValue = FALSE;
                    $field = new $fieldClass;
                    $field->name = $upperFieldName;
                    $field->fieldType = $fieldType;
                    if (property_exists($fieldClass, 'value')) {
                        $field->value = $newValue;
                    } elseif (property_exists($fieldClass, 'values')) {
                        if (!is_array($newValue))
                            $newValue = array_map('trim', explode(',', $newValue));
                        $field->values = $newValue;
                    }

                    $nullKey = array_search($upperFieldName, $this->nullFields, TRUE);
                    if ($nullKey !== FALSE) {
                        unset($this->nullFields[$nullKey]);
                    }
                    $this->fieldMap[$upperFieldName] = $field;
                    if (!in_array($upperFieldName, $this->dirtyFields, TRUE)) {
                        $this->dirtyFields[] = $upperFieldName;
                    }
                }
            }
        }

        /**
         * @param string $upperFieldName
         * @param mixed $value
         * @param string $fieldType
         * @return boolean
         */
        private function isFieldDirty($upperFieldName, $value, $fieldType = NULL) {
            if (!array_key_exists($upperFieldName, $this->fieldMap)) {
                if (in_array($upperFieldName, $this->nullFields, TRUE)) {
                    return $value !== NULL;
                } else {
                    return TRUE;
                }
            } else {
                $oldField = $this->fieldMap[$upperFieldName];
                if ($oldField === NULL) {
                    return TRUE;
                }
                $oldFieldType = $oldField->fieldType;
                if ($fieldType === NULL) {
                    $fieldType = $oldFieldType;
                }
                if ($oldFieldType !== $fieldType) {
                    return TRUE;
                }

                $oldValue = $this->getValue($upperFieldName);

                $newValue = $value;
                if (!is_array($oldValue) && is_array($newValue)) {
                    $newValue = implode(', ', $newValue);
                }

                $type = $this->getPHPTypeForFieldType($fieldType);
                $this->setPHPTypeOfValue($newValue, $type);

                return $oldValue !== $newValue;
            }
        }

        /**
         * @param string $fieldType
         * @return string
         */
        protected function getFieldClass($fieldType) {
            switch ($fieldType) {
                case 'BOOLEAN':
                    return 'de\cas\open\server\api\types\BooleanField';
                case 'BYTES':
                    return 'de\cas\open\server\api\types\BinaryField';
                case 'DATETIME':
                    return 'de\cas\open\server\api\types\DateField';
                case 'DECIMAL':
                    return 'de\cas\open\server\api\types\DecimalField';
                case 'DECIMALSUGGEST':
                    return 'de\cas\open\server\api\types\DecimalSuggestField';
                case 'DOUBLE':
                    return 'de\cas\open\server\api\types\DoubleField';
                case 'DOUBLESUGGEST':
                    return 'de\cas\open\server\api\types\DoubleSuggestField';
                case 'GGUID':
                    return 'de\cas\open\server\api\types\GGUIDField';
                case 'INT':
                    return 'de\cas\open\server\api\types\IntField';
                case 'INTSUGGEST':
                    return 'de\cas\open\server\api\types\IntSuggestField';
                case 'LONG':
                    return 'de\cas\open\server\api\types\LongField';
                case 'LONGSUGGEST':
                    return 'de\cas\open\server\api\types\LongSuggestField';
                case 'STRING':
                    return 'de\cas\open\server\api\types\StringField';
                case 'STRINGSUGGEST':
                    return 'de\cas\open\server\api\types\StringSuggestField';
                case 'STRINGLISTSUGGEST':
                    return 'de\cas\open\server\api\types\StringListSuggestField';
                case 'STRINGTREESUGGEST':
                    return 'de\cas\open\server\api\types\StringTreeSuggestField';
                case 'TEXT':
                    return 'de\cas\open\server\api\types\TextField';
                case 'CURRENCY':
                    return 'de\cas\open\server\api\types\CurrencyField';
                case 'CURRENCYSUGGEST':
                    return 'de\cas\open\server\api\types\CurrencySuggestField';
                case 'SELECTIONVALUELIST':
                    return 'de\cas\open\server\api\types\SelectionValueListField';
            }
            return NULL;
        }

        /**
         * Returns the PHP type for the given EIM type (for rows from IEIMInterface::query).
         *
         * @param string $fieldType Field type from CAS FieldDescription
         * @return string|null PHP type, or NULL
         */
        public static function getPHPTypeForFieldType($fieldType) {
            if (empty($fieldType))
                return NULL;

            $type = strtolower($fieldType);

            if (substr($type, -7) === 'suggest')
                $type = substr_replace($type, '', -7);

            switch ($type) {
                case 'boolean':
                    return 'boolean';
                case 'int':
                    return 'integer';
                case 'string':
                    return 'string';
                case 'double':
                    return 'float';
                # TODO: 'bytes'?
            }

            return NULL;
        }

        /**
         * @param mixed $value
         * @param string $type
         * @return void
         */
        public static function setPHPTypeOfValue(&$value, $type) {
            if ($value !== NULL && !empty($type)) {
                settype($value, $type);
            }
        }

        /**
         * @return DataObjectTransferable
         */
        public function toDataObjectTransferable() {
            $transferable = new DataObjectTransferable;
            $transferable->objectType = $this->objectType;
            $transferable->client = $this->client;
            $transferable->descriptionVersion = $this->descriptionVersion;

            $transferable->fields = array_values($this->fieldMap);

            $transferable->nullFields = array_values($this->nullFields);

            $transferable->permissions = array_values($this->permissions);
            $transferable->permissionsDirty = $this->permissionsDirty;

            $transferable->dirtyFields = array_values($this->dirtyFields);

            $transferable->foreignEditPermissionRestriction = $this->foreignEditPermissionRestriction;
            $transferable->writable = $this->writable;

            $transferable->localizedTags = array_values($this->tags);
            $transferable->tagsDirty = $this->tagsDirty;

            return $transferable;
        }

        /**
         * @return boolean
         */
        public function isDirty() {
            return !empty($this->dirtyFields) || $this->tagsDirty || $this->permissionsDirty ||
                $this->foreignEditPermissionRestriction != $this->foreignEditPermissionRestrictionOld;
        }

        /**
         * @return void
         */
        public function clearDirtyState() {
            $this->dirtyFields = array();
            $this->tagsDirty = FALSE;
            $this->permissionsDirty = FALSE;
            $this->foreignEditPermissionRestrictionOld = $this->foreignEditPermissionRestriction;
        }

        /**
         * @param string $upperFieldName
         * @return void
         */
        public function setFieldToNull($upperFieldName) {
            if (!in_array($upperFieldName, $this->nullFields, TRUE)) {
                $this->nullFields[] = $upperFieldName;
            }
            if (isset($this->fieldMap[$upperFieldName])) {
                unset($this->fieldMap[$upperFieldName]);
            }
            if (!in_array($upperFieldName, $this->dirtyFields, TRUE)) {
                $this->dirtyFields[] = $upperFieldName;
            }
        }

        /**
         * @param string $fieldName
         * @return string|NULL
         */
        public function getFieldType($fieldName) {
            $field = $this->getField($fieldName);
            if ($field === NULL) {
                return NULL;
            }
            return $field->fieldType;
        }

        /**
         * @param string $fieldName
         * @return void
         */
        public function removeField($fieldName) {
            $upperFieldName = strtoupper($fieldName);
            $nullKey = array_search($upperFieldName, $this->nullFields, TRUE);
            if ($nullKey !== FALSE) {
                unset($this->nullFields[$nullKey]);
            }
            if (isset($this->fieldMap[$upperFieldName])) {
                unset($this->fieldMap[$upperFieldName]);
            }
            $dirtyKey = array_search($upperFieldName, $this->dirtyFields, TRUE);
            if ($dirtyKey !== FALSE) {
                unset($this->dirtyFields[$dirtyKey]);
            }
        }

        /**
         * @param string $includeNullFields
         * @return array
         */
        public function getUsedFields($includeNullFields = TRUE) {
            $fields = array_keys($this->fieldMap);
            if ($includeNullFields) {
                foreach ($this->nullFields as $nullField) {
                    $fields[] = $nullField;
                }
            }
            return $fields;
        }

    }

}
