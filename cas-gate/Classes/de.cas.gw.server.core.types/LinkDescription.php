<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        DEPRECATED! Please use \de\cas\open\server\api\types\LinkDescriptions.
     *        Describes a link. The roles are translated to the session's locale.
     *	@see \de\cas\open\server\api\types\LinkDescriptions
     */
    class LinkDescription {

        /**
         * @var string
         *
         */
        public $attribute;

        /**
         * @var string
         *
         */
        public $objectTypeLeft;

        /**
         * @var string
         *
         */
        public $objectTypeRight;

        /**
         * @var boolean
         *
         */
        public $directed;

        /**
         * @var boolean
         *
         */
        public $deactivated;

        /**
         * @var boolean
         *
         */
        public $cardinalityLeftUnbounded;

        /**
         * @var boolean
         *
         */
        public $cardinalityRightUnbounded;

        /**
         * @var string
         *
         */
        public $roleLeft;

        /**
         * @var string
         *
         */
        public $roleRight;

    }

}
