<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A user can login and use the EIM server.
     */
    class User extends \de\cas\open\server\api\types\PermissionsOwner {

        /**
         * @var string
         *
         *										The GGUID of an address record, which contains
         *										additional user information like street and city.
         */
        public $guidOfAddressRecord;

        /**
         * @var string
         *
         *										The preferred language of the user.
         */
        public $preferredLanguage;

        /**
         * @var string
         *
         *										The preferred country of the user.
         */
        public $preferredCountry;

        /**
         * @var boolean
         *
         *										True if the user is an administrator.
         */
        public $administrator;

        /**
         * @var boolean
         *
         *										True if the user is an action administrator.
         */
        public $actionAdministrator;

        /**
         * @var string
         *
         *										Indicates if the user is a person or a resource.
         */
        public $userType;

        /**
         * @var string
         *
         *										The preferred time zone value of the user, i.e. "CET",
         *										"Europe/Berlin" or "America/Cancun".
         */
        public $timeZone;

        /**
         * @var boolean
         *
         *										Sets/Returns the flag that indicates whether the user
         *										is a guest. Guests have certain restrictions (e.g. they
         *										are not allowed to change their password).
         */
        public $Guest;

    }

}
