<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for a byte[] value in a generic DataObject
     */
    class BinaryField extends \de\cas\open\server\api\types\Field {

        /**
         * @var unknown
         *The byte[] value of this field
         */
        public $value;

    }

}
