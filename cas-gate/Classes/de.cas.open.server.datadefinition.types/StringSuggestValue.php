<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class StringSuggestValue extends \de\cas\open\server\datadefinition\types\SuggestValue {

        /**
         * @var boolean
         *
         */
        public $International;

        /**
         * @var array
         *
         */
        public $DisplayNames;

        /**
         * @var array
         *
         */
        public $Locales;

    }

}
