<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the available distribution lists.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetListsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetListsRequest
     */
    class GetListsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										MassQueryResult with the following columns:<br/>
         *										GGUID: GGUID<br/>
         *										KEYWORD: Keyword of the distributor<br/>
         *										OWNERNAME: ownername of the distributor<br/>
         *										COUNT: the number of address contained<br/>
         */
        public $result;

    }

}
