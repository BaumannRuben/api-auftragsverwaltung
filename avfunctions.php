<pre>
<?php
include 'cas-gate/CASGate.php';
use de\cas\open\server\addressesvalidation\types\FormatPhoneNumberRequest;

function createAddress($firma_gguid, $geschlecht, $vorname, $name, $email, $telefon, $mobil, $abteilung, $funktion)
{
    $addressdata = array();
    // Persönliche Daten
    if ($geschlecht == 'männlich') {
        $addressterm = 'Herrn';
        $addressletter = "Sehr geehrter Herr $name";
    } else if ($geschlecht == 'weiblich') {
        $addressterm = 'Frau';
        $addressletter = "Sehr geehrte Frau $name";
    } else if ($geschlecht == 'sonstige') {
        $addressterm = '';
        $addressletter = 'Sehr geehrte Damen und Herren';
    } else {
        $addressterm = '';
        $addressletter = '';
    }
    $addressdata['GWGENDER'] = array(
        'value' => $geschlecht,
        'fieldType' => 'STRING'
    );
    $addressdata['ADDRESSTERM'] = array(
        'value' => $addressterm,
        'fieldType' => 'STRING'
    );
    $addressdata['ADDRESSLETTER'] = array(
        'value' => $addressletter,
        'fieldType' => 'STRING'
    );
    $addressdata['CHRISTIANNAME'] = array(
        'value' => $vorname,
        'fieldType' => 'STRING'
    );
    $addressdata['NAME'] = array(
        'value' => $name,
        'fieldType' => 'STRING'
    );
    // E-Mail, Telefon & erlaubte Kontaktart
    $addressdata['MAILFIELDSTR1'] = array(
        'value' => $email,
        'fieldType' => 'STRING'
    );
    $addressdata['PHONEFIELDSTR4'] = array(
        'value' => formatPhoneNumber($telefon)->phoneNumber,
        'fieldType' => 'STRING'
    );
    $addressdata['PHONEFIELDSTR2'] = array(
        'value' => formatPhoneNumber($mobil)->phoneNumber,
        'fieldType' => 'STRING'
    );
    $allowedContact = array();
//     if (! empty(trim($relation['Address']))) {
//         array_push($allowedContact, "Postversand");
//     }
    if (! empty($email)) {
        array_push($allowedContact, "E-Mail");
    }
    if (! empty($telefon) || ! empty($mobil)) {
        array_push($allowedContact, "Telefon", "SMS");
    }
    $addressdata['GWSALESACTIVITYTYPEALLOWED'] = array(
        'value' => $allowedContact,
        'fieldType' => 'STRINGLISTSUGGEST'
    );
    $addressdata['DEPARTMENT'] = array(
        'value' => $abteilung,
        'fieldType' => 'STRING'
    );
    $addressdata['CASFUNCTION'] = array(
        'value' => $funktion,
        'fieldType' => 'STRING'
    );
    $addressdata['ISORGANISATION'] = array(
        'value' => false,
        'fieldType' => 'BOOLEAN'
    );
    $addressdata['PRIMARYORGANISATION'] = array(
        'value' => $firma_gguid,
        'fieldType' => 'GGUID'
    );
    $options = array("conditions" => array("AND" => array("CHRISTIANNAME" => $vorname, "NAME" =>  $name, "MAILFIELDSTR1" => $email)));
    $tempAddress = HochwarthIT_CASGate::getAdressDao()->find('first', $options);
    if (empty($tempAddress)) {
        $address = HochwarthIT_CASGate::getAdressDao()->createAddress($addressdata);
        return "Kontakt angelegt!<br>";
    }
    else {
        return "Kontakt schon vorhanden!<br>";
    }
}

function formatPhoneNumber($unformattedPhoneNumber, $countryCode = "de") {
    $formatPhoneNumberRequest = new FormatPhoneNumberRequest();
    $formatPhoneNumberRequest->phoneNumber = $unformattedPhoneNumber;
    $formatPhoneNumberRequest->countryCode = $countryCode;
//     print_r($formatPhoneNumberRequest);
    $phoneNumberResponse = HochwarthIT_CASGate::getEIMInterface()->execute($formatPhoneNumberRequest);
    
    return $phoneNumberResponse;
}

?>
</pre>