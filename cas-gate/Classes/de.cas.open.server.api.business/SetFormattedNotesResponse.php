<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\ResponseObject: Sets the html notes for the dataobject with the
     *        given GGUID and field name. Use with null content to delete the notes.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: SetFormattedNotesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SetFormattedNotesRequest
     */
    class SetFormattedNotesResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
