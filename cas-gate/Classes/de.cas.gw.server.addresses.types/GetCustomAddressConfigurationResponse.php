<?php
// This file has been automatically generated.

namespace de\cas\gw\server\addresses\types {

    /**
     * @package de\cas\gw\server\addresses
     * @subpackage types
     *
     *        Returns the custom address configuration.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetCustomAddressConfigurationRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetCustomAddressConfigurationRequest
     */
    class GetCustomAddressConfigurationResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Indicates if the custom adress type name feature is enabled.
         */
        public $isCustomAddressTypeNameActivated;

        /**
         * @var int
         *
         *                    The count of the address types.
         */
        public $customAddressTypeCount;

        /**
         * @var array
         *
         *                    The dictionary entries.
         */
        public $entries;

    }

}
