<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject:Retrieves the country code for
     *        public holidays configured for a specific user given by GUID.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetPublicHolidayCountryCodeForUserResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPublicHolidayCountryCodeForUserResponse
     */
    class GetPublicHolidayCountryCodeForUserRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                  guid of the user for that the countryCode is needed
         */
        public $userGuid;

    }

}
