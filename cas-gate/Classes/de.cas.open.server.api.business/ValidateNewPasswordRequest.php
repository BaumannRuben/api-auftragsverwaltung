<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the \de\cas\open\server\business\BusinessOperation that
     * 				validates if the given password is valid.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				ValidateNewPasswordResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ValidateNewPasswordResponse
     */
    class ValidateNewPasswordRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Password to validate.
         */
        public $passwordToValdiate;

        /**
         * @var string
         *
         *                    Username (login name) of user, i.e. 'Robert Glaser'
         */
        public $username;

    }

}
