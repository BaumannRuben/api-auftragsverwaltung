<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *A common super class for User and Group.
     */
    class PermissionsOwner {

        /**
         * @var int
         *
         *								An internal ID in the database for
         *								faster access.
         */
        public $id;

        /**
         * @var string
         *
         *								The unique gguid of the permission
         *								owner. It is also unique for all clients.
         */
        public $GGUID;

        /**
         * @var string
         *
         *								The displayname of the permission owner
         *								as it is displayed in the GUI. Examples:
         *								"Robert Glaser", "trainees"
         */
        public $displayName;

        /**
         * @var string
         *
         *								The client name (German: "Mandant"),
         *								i.e. "CAS".
         */
        public $client;

        /**
         * @var string
         *
         *								The description of the permission owner.
         */
        public $description;

        /**
         * @var boolean
         *
         *								Indicates if the permission owner is
         *								enabled.
         */
        public $enabled;

        /**
         * @var array
         *
         *								List of group ids this permission owner
         *								is a member of.
         */
        public $directSuperGroups;

        /**
         * @var string
         *
         *								Case User: The email address of the
         *								user. This is only a fallback, it is
         *								recommended to use the address record to
         *								retrieve the preferred contact type and
         *								address. Case Group: E-Mail address for
         *								mailing lists.
         */
        public $emailAddress;

    }

}
