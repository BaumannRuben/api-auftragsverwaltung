<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves DataObjects and LinkObjects within a transaction. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: SaveObjectsAndLinksResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveObjectsAndLinksResponse
     */
    class SaveObjectsAndLinksRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\MassUpdateData
         *
         */
        public $dataObjects;

        /**
         * @var array
         *
         */
        public $linkObjects;

    }

}
