<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     */
    class XrmType {

        /**
         * @var string
         *
         *                The GGUID of the object.
         */
        public $GGUID;

        /**
         * @var string
         *
         *                The name of the xrm type.
         */
        public $name;

        /**
         * @var string
         *
         *                The DataObjectType of the xrm type.
         */
        public $dataObjectType;

    }

}
