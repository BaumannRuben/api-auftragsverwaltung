<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Imports a vcard address by parsing the given byte array.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: ImportVCardAddressResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ImportVCardAddressResponse
     */
    class ImportVCardAddressRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *                    Sets/Returns the content of the vcard.
         */
        public $vcardContent;

        /**
         * @var string
         *The charset to use.
         */
        public $charset;

        /**
         * @var boolean
         *
         *                    Indicates if the imported records should be
         *                    private i.e. only visible to the current user.
         *                    NOTE: If 'importAsPrivate' is set to 'TRUE',
         *                    'importAsPublic' is ignored.
         */
        public $importAsPrivate;

        /**
         * @var boolean
         *
         *                    Indicates if the imported records should be
         *                    visible to all users.
         *                    NOTE: If 'importAsPrivate' is set to 'TRUE',
         *                    'importAsPublic' is ignored.
         */
        public $importAsPublic;

        /**
         * @var string
         *
         *                    Defines whether the contacts from the vCard should all be
         *                    treated as single contacts or organizations or whether the
         *                    BO should decide individually. See VCardImportMode
         *                    for possible values. The default is {@link
         *                    VCardImportMode#MIXED}.
         *	@see VCardImportMode
         */
        public $importMode;

    }

}
