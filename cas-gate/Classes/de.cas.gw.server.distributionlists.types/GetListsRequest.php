<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the available distribution lists.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetListsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetListsResponse
     */
    class GetListsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
