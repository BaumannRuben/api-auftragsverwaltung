<pre>
<?PHP
include 'cas-gate/CASGate.php';

/* Datenbankverbindung herstellen */
$con = new mysqli('192.168.40.13', 'root', 'root', 'hochwarth-zeit');
$con->set_charset('utf8');
if ($con->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $con->connect_error);
} else {
    echo "Verbindung erfolgreich hergestellt!<br><br>";
}

$datei = fopen("logs\\kuerzel_" . date("Y-m-d-H-i-s") . ".txt", "w");

$anzKunden = 0;
$anzKundenNr = 0;
$anzKundenNotGW = 0;
$anzUpdate = 0;
$anzNoUpdate = 0;

// Hole alle Adressen aus der AV
$sqlkunden = $con->query('SELECT * FROM tblkunden ORDER BY KundeNr;');
$avkunden = $sqlkunden->fetch_all(MYSQLI_ASSOC);
foreach ($avkunden as $avkunde) {
    fwrite($datei, "Kunde: ".utf8_decode($avkunde['Firma']));
    $anzKunden ++;
    // Kundennummer in AV vorhanden? -> sollte immer wahr sein
    if (! empty($avkunde['KundeNr'])) {
        $anzKundenNr ++;
        // Kunden aus CAS genesisWorld laden
        $option['conditions'] = array(
            'AND' => array(
                'HOCHWARTH_KUNDENNUMMER' => $avkunde['KundeNr'],
                'GWISCOMPANY' => 1,
                'GWISCONTACT' => 0,
				'GWDEACTIVATED' => 0
            )
        );
        $kunde = HochwarthIT_CASGate::getAdressDao()->find('first', $option);
        if (! empty($kunde)) {
            fwrite($datei, " in CAS vorhanden!\r\n");
            $kuerzel = $kunde['KUERZEL']['value'];
            $kundennummer = $kunde['HOCHWARTH_KUNDENNUMMER']['value'];
            fwrite($datei, "--> KNR: $kundennummer K�rzel: $kuerzel");
            // Aenderung?
            if ($avkunde['Kurz'] != $kuerzel) {
                $sql_update = "UPDATE tblkunden SET Kurz ='$kuerzel' WHERE KundeNr = $kundennummer;";
                $result = $con->query($sql_update);
                if ($result == true) {
                    $anzUpdate ++;
                    fwrite($datei, " ... aktualisiert!\r\n");
                }
            } else {
                $anzNoUpdate ++;
                fwrite($datei, " ... nicht aktualisiert!\r\n");
            }
			//Umsatz laden und speichern
			$umsaetze = getUmsatz($con, $avkunde['KundeNr']);
            $tempkunde = HochwarthIT_CASGate::getAdressDao()->load($kunde['GGUID']['value']);
            $tempkunde->setValue($umsaetze['Gesamt'], 'E_UMSATZ', 'DECIMAL');
            $tempkunde->setValue($umsaetze['Aktuelles Jahr'], 'E_UMSATZ_AKT', 'DECIMAL');
            $tempkunde->setValue($umsaetze['Letztes Jahr'], 'E_UMSATZ_PREV', 'DECIMAL');
            $tempkunde = HochwarthIT_CASGate::getAdressDao()->save($tempkunde);
        }
        else {
            $anzKundenNotGW++;
            fwrite($datei, " in CAS  nicht vorhanden!\r\n");
        }
    }
}


echo "Kunden: ".$anzKunden."<br>";
echo "Kunden mit KundeNr: ".$anzKundenNr."<br>";
echo "Kunden nicht in CAS: ".$anzKundenNotGW."<br>";
echo "Kuerzel aktualisiert: ".$anzUpdate."<br>";
echo "Kuerzel nicht aktualisiert: ".$anzNoUpdate."<br>";
fwrite($datei,  "Kunden: $anzKunden\r\n");
fwrite($datei,  "Kunden mit KundeNr: $anzKundenNr\r\n");
fwrite($datei,  "Kunden nicht in CAS: $anzKundenNotGW\r\n");
fwrite($datei,  "Kuerzel aktualisiert: $anzUpdate\r\n");
fwrite($datei,  "Kuerzel nicht aktualisiert: $anzNoUpdate\r\n");

fclose($datei);

$con->close();

function getUmsatz($con, $kundennr) {
    $aktYearStart = date("Y")."-01-01";
    $aktYearEnd = date("Y-m-d");
    $prevYearStart = (date("Y")-1)."-01-01";
    $prevYearEnd = (date("Y")-1)."-12-31";
    $umsaetze = array();
    $sqlumsatz1 = $con->query("SELECT sum(Anzahl*VK) AS Umsatz FROM tblware, tblauftraege WHERE KundeNr = $kundennr AND tblware.AuftragNr = tblauftraege.AuftragNr;");
    $avumsatz1 = $sqlumsatz1->fetch_all(MYSQLI_ASSOC);
    $sqlumsatz2 = $con->query("SELECT sum(Anzahl*Satz) AS Umsatz FROM tblstunden, tblauftraege WHERE KundeNr = $kundennr AND tblstunden.AuftragNr = tblauftraege.AuftragNr;");
    $avumsatz2 = $sqlumsatz2->fetch_all(MYSQLI_ASSOC);
    $umsaetze['Gesamt'] = $avumsatz1[0]['Umsatz'] + $avumsatz2[0]['Umsatz'];
    $sqlumsatz3 = $con->query("SELECT sum(Anzahl*VK) AS Umsatz FROM tblware, tblauftraege WHERE KundeNr = $kundennr AND tblware.AuftragNr = tblauftraege.AuftragNr AND tblware.Datum >= '$aktYearStart' AND tblware.Datum <= '$aktYearEnd';");
    $avumsatz3 = $sqlumsatz3->fetch_all(MYSQLI_ASSOC);
    $sqlumsatz4 = $con->query("SELECT sum(Anzahl*Satz) AS Umsatz FROM tblstunden, tblauftraege WHERE KundeNr = $kundennr AND tblstunden.AuftragNr = tblauftraege.AuftragNr AND tblstunden.Datum >= '$aktYearStart' AND tblstunden.Datum <= '$aktYearEnd';");
    $avumsatz4 = $sqlumsatz4->fetch_all(MYSQLI_ASSOC);
    $umsaetze['Aktuelles Jahr'] = $avumsatz3[0]['Umsatz'] + $avumsatz4[0]['Umsatz'];
    $sqlumsatz5 = $con->query("SELECT sum(Anzahl*VK) AS Umsatz FROM tblware, tblauftraege WHERE KundeNr = $kundennr AND tblware.AuftragNr = tblauftraege.AuftragNr AND tblware.Datum >= '$prevYearStart' AND tblware.Datum <= '$prevYearEnd';");
    $avumsatz5 = $sqlumsatz5->fetch_all(MYSQLI_ASSOC);
    $sqlumsatz6 = $con->query("SELECT sum(Anzahl*Satz) AS Umsatz FROM tblstunden, tblauftraege WHERE KundeNr = $kundennr AND tblstunden.AuftragNr = tblauftraege.AuftragNr AND tblstunden.Datum >= '$prevYearStart' AND tblstunden.Datum <= '$prevYearEnd';");
    $avumsatz6 = $sqlumsatz6->fetch_all(MYSQLI_ASSOC);
    $umsaetze['Letztes Jahr'] = $avumsatz5[0]['Umsatz'] + $avumsatz6[0]['Umsatz'];
    return $umsaetze;
}
?>
</pre>