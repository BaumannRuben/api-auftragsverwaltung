<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the \de\cas\open\server\business\BusinessOperation that deletes users, groups or resources.<br />
     *					Corresponding \de\cas\open\server\api\types\RequestObject: DeletePermissionsOwnersRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeletePermissionsOwnersRequest
     */
    class DeletePermissionsOwnersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *Exceptions for the not existing and so not
         *										deletable permission owners.
         */
        public $failures;

    }

}
