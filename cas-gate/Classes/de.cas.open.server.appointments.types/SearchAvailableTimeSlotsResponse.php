<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Searches for a free time period of a given length in a
     *				given timespan for a list of given users. Returns matching time periods found
     *				and a proposed appointment. Corresponding \de\cas\open\server\api\types\RequestObject: SearchAvailableTimeSlotsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SearchAvailableTimeSlotsRequest
     */
    class SearchAvailableTimeSlotsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Provides access to the list holding all available time
         *										slots
         */
        public $availableTimeSlots;

    }

}
