<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a foreign edit permission a rightsowner identified by
     *				grantingRightOwnerID grants to another rightsowner identified by accessorID.
     *				IDs of user (OID) are usually positive while groups have negative IDs (GID).
     *				See \de\cas\open\server\dataaccess\PermissionsProvider for details.
     *	@see \de\cas\open\server\dataaccess\PermissionsProvider
     */
    class ForeignEditPermission {

        /**
         * @var string
         *
         *								GGUID of foreign edit permission in
         *								table SysUserRelation
         */
        public $GGUID;

        /**
         * @var int
         *
         *								The rightsowner granting the
         *								foreigneditpermission
         */
        public $grantingRightOwnerID;

        /**
         * @var int
         *
         *								The permissions owner to whom the
         *								foreigneditpermission was granted.
         */
        public $accessorID;

        /**
         * @var int
         *
         *								The granted right
         */
        public $editPermission;

        /**
         * @var string
         *
         *								The objecttype (e.g. "Address") or NULL
         *								if this right is the STANDARD
         *								permission.
         */
        public $objectType;

    }

}
