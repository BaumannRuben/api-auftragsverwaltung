<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject
     *        of the business operation that validates an IBAN.
     *        Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: ValidateIbanResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ValidateIbanResponse
     */
    class ValidateIbanRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The IBAN to be validated. Mandatory.
         */
        public $iban;

        /**
         * @var string
         *
         *                    The ISO code of the country. Mandatory.
         */
        public $countryCode;

    }

}
