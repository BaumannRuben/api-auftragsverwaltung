<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Response object for the Warmup request.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: WarmupRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see WarmupRequest
     */
    class WarmupResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
