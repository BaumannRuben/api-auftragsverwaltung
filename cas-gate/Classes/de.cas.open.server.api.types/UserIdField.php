<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for an ID identifying a user or a group.
     */
    class UserIdField extends \de\cas\open\server\api\types\IntField {

    }

}
