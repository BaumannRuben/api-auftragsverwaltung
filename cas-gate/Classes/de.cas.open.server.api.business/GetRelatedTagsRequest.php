<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Requestobject for the businessoperation that returns all related tags to a
     *				given objecttype and list of GGUIDs of this type. Corresponding \de\cas\open\server\api\types\ResponseObject: \de\cas\eim\api\business\GetRelatedTagsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\eim\api\business\GetRelatedTagsResponse
     */
    class GetRelatedTagsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Sets/Returns the objectType.
         */
        public $objectType;

        /**
         * @var array
         *
         *										Returns a list of GGUIDs of \de\cas\open\server\api\types\DataObject.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $dataObjectGGUIDs;

    }

}
