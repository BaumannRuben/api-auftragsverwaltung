<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business
     *				operation that updates the customer's database schema
     * 				according to a given \de\cas\open\server\datadefinition\types\ObjectTypeDefinition.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				UpdateDatabaseSchemaResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\datadefinition\types\ObjectTypeDefinition
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see UpdateDatabaseSchemaResponse
     */
    class UpdateDatabaseSchemaRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\datadefinition\types\ObjectTypeDefinition
         *
         *                    Describes the new nominal state
         *                    of the objecttype.
         */
        public $newObjectTypeDefinition;

        /**
         * @var array
         *
         *                    The dictionary entries to save.
         */
        public $dictionaryEntries;

        /**
         * @var array
         *
         */
        public $columnsToDelete;

    }

}
