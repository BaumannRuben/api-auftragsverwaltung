<pre>
<?php
include 'cas-gate/CASGate.php';

$anzTickets = 0;
$anzNew = 0;
$anzUpdate = 0;
$anzVk = 0;
$anzNotVk = 0;
$nichtKNR = array();

$datei = fopen("logs\\tasks\\aufgaben_" . date("Y-m-d-H-i-s") . ".txt", "w");

// $con = new mysqli('192.168.40.13', 'root', 'root', 'hochwarth-zeit');
$con = new mysqli('localhost', 'root', '', 'hochwarth-zeit');
$con->set_charset('utf8');
if ($con->connect_errno) {
    fwrite($datei, "Verbindung fehlgeschlagen!\r\n");
    die("Verbindung fehlgeschlagen: " . $con->connect_error);
} else {
    fwrite($datei, "Verbindung erfolgreich!\r\n");
    fwrite($datei, "Beginn: ".date("Y-m-d H:i:s")."\r\n");
    echo "Verbindung erfolgreich hergestellt!<br><br>";
}

//Alle Aufgaben aus der AV laden
$sql_afg = $con->query("SELECT * FROM tblaufgaben WHERE readout != 1;");
$aufgaben = $sql_afg->fetch_all(MYSQLI_ASSOC);
foreach ($aufgaben as $aufgabe) {
    $anzTickets++;
    fwrite($datei, $aufgabe['AufgabeNr']."\r\n");

    // Pruefung ob Vorgang schon existiert
    $option['conditions'] = array(
        'E_AV_NUMMER' => $aufgabe['AufgabeNr']
    );
    $vorgang = HochwarthIT_CASGate::getTaskDao()->find('first', $option);     
    if(!empty($vorgang))
    {
        //Bestehenden Vorgang aktualisieren (ausgenommen Typ, E_AV_NUMMER und E_AVKNDNR)
        //Verknuepfung wird nicht angefasst
        $temp_vorgang = HochwarthIT_CASGate::getTaskDao()->load($vorgang['GGUID']['value']);
        
        $temp_vorgang->setValue($aufgabe['Bezeichnung'],'KEYWORD','STRING');
        $temp_vorgang->setValue(str_replace('', '', $aufgabe['Beschreibung']),'ITDPROBLEM','STRING');
        $temp_vorgang->setValue($aufgabe['Prio'],'GWSPRIORITY','STRING');
        
        $sql_ma = $con->query("SELECT MitarbeiterNr, Vorname, Name FROM tblmitarbeiter WHERE MitarbeiterNr = " . $aufgabe['Ursprung'] . ";");
        $ursprung = $sql_ma->fetch_all(MYSQLI_ASSOC);
        $temp_vorgang->setValue($ursprung[0]['Vorname'] . " " . $ursprung[0]['Name'],'E_MITARBEITER_URSPRUNG','STRING');
        
        $sql_va = $con->query("SELECT MitarbeiterNr, Vorname, Name FROM tblmitarbeiter WHERE MitarbeiterNr = " . $aufgabe['MitarbeiterNr'] . ";");
        $verantwortlicher = $sql_va->fetch_all(MYSQLI_ASSOC);
        $temp_vorgang->setValue($verantwortlicher[0]['Vorname'] . " " . $verantwortlicher[0]['Name'],'E_MITARBEITER_VERANTWORTUNG','STRING');

        $temp_vorgang->setValue($aufgabe['Abteilung'],'E_MITARBEITER_TEAM','STRING');
//         $temp_vorgang->setValue($aufgabe['Historie'],'NOTES2','STRING');
        $temp_vorgang->setValue(str_replace('', '', $aufgabe['Kundeninfos']),'INTNOTES','STRING');
        $temp_vorgang->setValue($aufgabe['Status'],'GWSSTATUS','STRING');
        
        //Vergleich der Zeiten, wenn Ende frueher als Beginn, dann wird alles auf Beginn gesetzt
        $temp_vorgang->setValue(date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt'])),'START_DT','DATETIME');
        if (date('Y-m-d\TH:i:s', strtotime($aufgabe['ReaktionBis'])) >= date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt']))) {
            $temp_vorgang->setValue(date('Y-m-d\TH:i:s', strtotime($aufgabe['ReaktionBis'])),'TORESPONSETIME','DATETIME');
        }
        else {
            $temp_vorgang->setValue(date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt'])),'TORESPONSETIME','DATETIME');
        }
        
        if(!empty($aufgabe['Abschluss']) && date('Y-m-d\TH:i:s', strtotime($aufgabe['Abschluss'])) >= date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt']))) {
            $temp_vorgang->setValue(date('Y-m-d\TH:i:s', strtotime($aufgabe['Abschluss'])),'END_DT','DATETIME');
        }
        
        
        $sql_gesellschaft = $con->query("SELECT Name FROM tblgesellschaften WHERE GesellschaftNr = ".$aufgabe['Gesellschaft'].";");
        $gesellschaft = $sql_gesellschaft->fetch_all(MYSQLI_ASSOC);
        $temp_vorgang->setValue($gesellschaft[0]['Name'],'E_GESELLSCHAFT','STRING');
        
        $temp_vorgang->setValue($aufgabe['Prio'],'GWSPRIORITY','STRING');
        $temp_vorgang->setValue($aufgabe['Bezeichnung'],'KEYWORD','STRING');
        $temp_vorgang->setValue($aufgabe['Prio'],'GWSPRIORITY','STRING');

        $temp_vorgang->setValue($aufgabe['ReaktionTyp'],'E_REAKTION_TYP','STRING');
        $temp_vorgang->setValue($aufgabe['ZeitbudgetA'],'E_ZEITBUDGETA','INT');
        
        $temp_vorgang = HochwarthIT_CASGate::getTaskDao()->save($temp_vorgang);
        fwrite($datei, "-> Aktualisierung!");
        $anzUpdate++;
        
        // Vorgang mit Kunde verknuepfen
        $option_adr['conditions'] = array(
        'AND' => array(
        'HOCHWARTH_KUNDENNUMMER' => $aufgabe['KundeNr'],
        'GWISCOMPANY' => 1,
        'GWISCONTACT' => 0
        )
        );
        $address = HochwarthIT_CASGate::getAdressDao()->find('first', $option_adr);
        if(!empty($address)) {
            HochwarthIT_CASGate::getTaskDao()->createLinkBetweenTaskAddress($temp_vorgang->getValue('GGUID'), $address['GGUID']['value']);
            fwrite($datei, " -> Verkn�pfung! [AV-KNR: ".$aufgabe['KundeNr']."]\r\n");
            $anzVk++;
        }
        else {
            fwrite($datei, " -> Verkn�pfung fehlgeschlagen! [AV-KNR: ".$aufgabe['KundeNr']."]\r\n");
            array_push($nichtKNR, $aufgabe['KundeNr']);
            $anzNotVk++;
        }
    }
    else {
        //Neuer Vorgang erzeugen
        $afgdata = array();

        $afgdata['KEYWORD'] = array(
            'value' => $aufgabe['Bezeichnung'],
            'fieldType' => 'STRING'
        );
        $afgdata['ITDPROBLEM'] = array(
            'value' => str_replace('', '', $aufgabe['Beschreibung']),
            'fieldType' => 'STRING'
        );
        //TODO Mapping Priorit�t
        $afgdata['GWSPRIORITY'] = array(
            'value' => $aufgabe['Prio'],
            'fieldType' => 'STRING'
        );
        $sql_ma = $con->query("SELECT MitarbeiterNr, Vorname, Name FROM tblmitarbeiter WHERE MitarbeiterNr = " . $aufgabe['Ursprung'] . ";");
        $ursprung = $sql_ma->fetch_all(MYSQLI_ASSOC);
        $afgdata['E_MITARBEITER_URSPRUNG'] = array(
            'value' => $ursprung[0]['Vorname'] . " " . $ursprung[0]['Name'],
            'fieldType' => 'STRING'
        );

        $sql_va = $con->query("SELECT MitarbeiterNr, Vorname, Name FROM tblmitarbeiter WHERE MitarbeiterNr = " . $aufgabe['MitarbeiterNr'] . ";");
        $verantwortlicher = $sql_va->fetch_all(MYSQLI_ASSOC);
        $afgdata['E_MITARBEITER_VERANTWORTUNG'] = array(
            'value' => $verantwortlicher[0]['Vorname'] . " " . $verantwortlicher[0]['Name'],
            'fieldType' => 'STRING'
        );
        $afgdata['E_MITARBEITER_TEAM'] = array(
            'value' => $aufgabe['Abteilung'],
            'fieldType' => 'STRING'
        );
//         $afgdata['NOTES2'] = array(
//             'value' => $aufgabe['Historie'],
//             'fieldType' => 'STRING'
//         );
        $afgdata['INTNOTES'] = array(
            'value' => str_replace('', '', $aufgabe['Kundeninfos']),
            'fieldType' => 'STRING'
        );

        $afgdata['GWSTYPE'] = array(
            'value' => 'Support',
            'fieldType' => 'STRING'
        );
        // TODO Mapping auf Status in Tickets
        $afgdata['GWSSTATUS'] = array(
            'value' => $aufgabe['Status'],
            'fieldType' => 'STRING'
        );

        $afgdata['START_DT'] = array(
            'value' => date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt'])),
            'fieldType' => 'DATETIME'
        );
        
        //Vergleich der Zeiten, wenn Ende frueher als Beginn, dann wird alles auf Beginn gesetzt
        if (date('Y-m-d\TH:i:s', strtotime($aufgabe['ReaktionBis'])) >= date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt']))) {
            $afgdata['TORESPONSETIME'] = array(
                'value' => date('Y-m-d\TH:i:s', strtotime($aufgabe['ReaktionBis'])),
                'fieldType' => 'DATETIME'
            );
        }
        else {
            $afgdata['TORESPONSETIME'] = array(
                'value' => date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt'])),
                'fieldType' => 'DATETIME'
            );
        }
        
        if(!empty($aufgabe['Abschluss']) && date('Y-m-d\TH:i:s', strtotime($aufgabe['Abschluss'])) >= date('Y-m-d\TH:i:s', strtotime($aufgabe['Zeitpunkt']))) {
            $afgdata['END_DT'] = array(
                'value' => date('Y-m-d\TH:i:s', strtotime($aufgabe['Abschluss'])),
                'fieldType' => 'DATETIME'
            );
        }
        
        $afgdata['E_AV_NUMMER'] = array(
            'value' => $aufgabe['AufgabeNr'],
            'fieldType' => 'STRING'
        );
        $afgdata['E_AV_KNDNR'] = array(
            'value' => $aufgabe['KundeNr'],
            'fieldType' => 'STRING'
        );

        $sql_gesellschaft = $con->query("SELECT Name FROM tblgesellschaften WHERE GesellschaftNr = ".$aufgabe['Gesellschaft'].";");
        $gesellschaft = $sql_gesellschaft->fetch_all(MYSQLI_ASSOC);
        $afgdata['E_GESELLSCHAFT'] = array(
            'value' => $gesellschaft[0]['Name'],
            'fieldType' => 'STRING'
        );

        $afgdata['E_REAKTION_TYP'] = array(
        'value' => $aufgabe['ReaktionTyp'],
        'fieldType' => 'STRING'
        );

        $afgdata['E_ZEITBUDGETA'] = array(
        'value' => $aufgabe['ZeitbudgetA'],
        'fieldType' => 'INT'
        );

        $newTask = HochwarthIT_CASGate::getTaskDao()->create($afgdata);
        fwrite($datei, "-> Erzeugung!");
        $anzNew++;

        // Vorgang mit Kunde verknuepfen
        $option_adr['conditions'] = array(
            'AND' => array(
                'HOCHWARTH_KUNDENNUMMER' => $aufgabe['KundeNr'],
                'GWISCOMPANY' => 1,
                'GWISCONTACT' => 0
            )
        );
        $address = HochwarthIT_CASGate::getAdressDao()->find('first', $option_adr);  
        if(!empty($address)) {
            HochwarthIT_CASGate::getTaskDao()->createLinkBetweenTaskAddress($newTask->getValue('GGUID'), $address['GGUID']['value']);
            fwrite($datei, " -> Verkn�pfung! [AV-KNR: ".$aufgabe['KundeNr']."]\r\n");
            $anzVk++;
        }
        else {
            fwrite($datei, " -> Verkn�pfung fehlgeschlagen! [AV-KNR: ".$aufgabe['KundeNr']."]\r\n");
            array_push($nichtKNR, $aufgabe['KundeNr']);
            $anzNotVk++;
        }
    }
    //readout setzen
    if($aufgabe['Status']=='abgeschlossen') {
        $con->query("UPDATE tblaufgaben SET readout = 1 WHERE AufgabeNr = ".$aufgabe['AufgabeNr'].";");
    }
}

fwrite($datei, "Tickets: $anzTickets\r\n");
fwrite($datei, "New: $anzNew\r\n");
fwrite($datei, "Update: $anzUpdate\r\n");
fwrite($datei, "VK: $anzVk\r\n");
fwrite($datei, "Not VK: $anzNotVk\r\n");
foreach($nichtKNR as $nKNR) {
    fwrite($datei, "Keine KNR in CAS: $nKNR\r\n");
}
fwrite($datei, "Ende: ".date("Y-m-d H:i:s")."\r\n");
fclose($datei);

$con->close();
echo "FERTIG";

?>
</pre>