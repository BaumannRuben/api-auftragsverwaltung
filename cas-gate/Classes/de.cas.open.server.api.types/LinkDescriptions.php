<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *						Describes possible links for an object type.
     */
    class LinkDescriptions {

        /**
         * @var string
         *
         *								The target object type for the links.
         */
        public $targetObjectType;

        /**
         * @var array
         *
         *								Lists all available link attributes for the given object type.
         */
        public $linkAttributes;

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *											The attribute used in the LinkObject.
         *	@see LinkObject
         */
        public $linkAttribute;

        /**
         * @var string
         *
         *											Describes the meaning of the linkAttribute.
         */
        public $semantics;

        /**
         * @var string
         *
         *											The link's direction from the perspective of the source object.
         */
        public $linkDirection;

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *											Possible role of the source object.
         */
        public $sourceRole;

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *											Possible role of the target object.
         */
        public $targetRole;

        /**
         * @var boolean
         *
         *                      Links, which were created by the administrator, can be deactivated. If a link is deactivated,
         *                      it no longer appears in the GUI and hence such links can no longer be created. Existing links
         *                      are not deleted.
         */
        public $deactivated;

        /**
         * @var boolean
         *
         *											Can have multiple links to source object type.
         */
        public $cardinalitySourceUnbounded;

        /**
         * @var boolean
         *
         *											Can have multiple links to target object type.
         */
        public $cardinalityTargetUnbounded;

    }

}
