<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object of the business operation that saves a user's
     *				favourite permissions owners (user, resource, group).
     *				Corresponding \de\cas\open\server\api\types\RequestObject: SaveFavouritePermissionsOwnersRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveFavouritePermissionsOwnersRequest
     */
    class SaveFavouritePermissionsOwnersResponse extends \de\cas\open\server\api\business\EmptyResponse {

    }

}
