<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *Class representing a team select result item
     */
    class TeamSelectResultItem {

        /**
         * @var string
         *GGUID of the right owner item
         */
        public $GGUID;

        /**
         * @var string
         *Type of the right owner
         */
        public $RightOwnerType;

        /**
         * @var unknown
         *Id of the right owner (OID)
         */
        public $RightOwnerId;

        /**
         * @var string
         *GUID of the domain containing the right owner
         */
        public $DomainGuid;

        /**
         * @var string
         *Display name of the right owner
         */
        public $DisplayName;

        /**
         * @var string
         *Description of the right owner
         */
        public $Description;

        /**
         * @var string
         *Address record guid of the right owner
         */
        public $AddressGuid;

        /**
         * @var string
         *Image guid of the right owner
         */
        public $ImageGuid;

        /**
         * @var string
         *Gender of the right owner
         */
        public $Gender;

        /**
         * @var unknown
         *Birthday of the right owner
         */
        public $Birthday;

    }

}
