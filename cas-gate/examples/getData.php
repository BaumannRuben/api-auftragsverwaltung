<?php

#Including CAS-Gate
require "CASGate.php";

$options = array(
    "conditions" => array(
        "AND" => array(
            array('CHRISTIANNAME' => "Max"),
            "OR" => array(
                array('NAME' => "Mustermann"),
                array('NAME' => "Mustermann1337")
            )

        )
    )
);
$addresses = HochwarthIT_CASGate::getAdressDao()->getAddresses($options);
print_r($addresses);