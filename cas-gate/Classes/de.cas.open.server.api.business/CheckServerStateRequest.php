<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: The request-object to check the health
     *				state of the server.
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class CheckServerStateRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
