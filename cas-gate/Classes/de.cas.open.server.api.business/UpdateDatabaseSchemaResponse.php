<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business
     *				operation that updates the customer's database schema
     * 				according to a given \de\cas\open\server\datadefinition\types\ObjectTypeDefinition.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: UpdateDatabaseSchemaRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\datadefinition\types\ObjectTypeDefinition
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see UpdateDatabaseSchemaRequest
     */
    class UpdateDatabaseSchemaResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
