<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> See request object.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetNewCallerIdRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetNewCallerIdRequest
     */
    class GetNewCallerIdResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										The new caller ID or the old one, if the user already
         *										had one.
         */
        public $callerId;

    }

}
