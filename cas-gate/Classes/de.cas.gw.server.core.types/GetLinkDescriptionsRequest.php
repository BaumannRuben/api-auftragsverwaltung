<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        DEPRECATED! The LinkDescriptions are part of the \de\cas\open\server\api\types\DataObjectDescriptionTransferable.
     *        \de\cas\open\server\api\types\RequestObject: Returns the a list of LinkDescriptions for the given object type.
     *        \de\cas\open\server\api\types\ResponseObject: GetLinkDescriptionsResponse
     *	@see \de\cas\open\server\api\types\DataObjectDescriptionTransferable
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see LinkDescription
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetLinkDescriptionsResponse
     */
    class GetLinkDescriptionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $objectType;

    }

}
