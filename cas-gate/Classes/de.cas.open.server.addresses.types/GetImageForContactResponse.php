<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves the image bytes for a given address.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetImageForContactRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetImageForContactRequest
     */
    class GetImageForContactResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    Sets/Returns the bytes of the image.
         */
        public $ImageBytes;

        /**
         * @var string
         *
         *                    Sets/Returns the file extension for the image, e.g.
         *                    'jpg' or 'bmp'.
         */
        public $ImageFileExtension;

    }

}
