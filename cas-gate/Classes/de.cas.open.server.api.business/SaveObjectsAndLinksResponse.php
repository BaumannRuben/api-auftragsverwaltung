<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves DataObjects and LinkObjects within a transaction. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: SaveObjectsAndLinksRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveObjectsAndLinksRequest
     */
    class SaveObjectsAndLinksResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $dataObjectExceptions;

        /**
         * @var array
         *
         */
        public $linkObjectExceptions;

    }

}
