<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Contains information about the external owners update.
     */
    class UpdateInvitationStatus {

        /**
         * @var string
         *
         */
        public $EmailAddress;

        /**
         * @var string
         *
         */
        public $InvitationStatus;

    }

}
