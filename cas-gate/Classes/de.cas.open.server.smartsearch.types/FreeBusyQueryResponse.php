<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the free/busy query result.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: FreeBusyQueryRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see FreeBusyQueryRequest
     */
    class FreeBusyQueryResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *Sets/Gets the right owners in the query result.
         */
        public $Results;

    }

}
