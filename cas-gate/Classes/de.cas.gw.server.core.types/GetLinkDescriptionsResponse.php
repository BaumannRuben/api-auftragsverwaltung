<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        DEPRECATED! The LinkDescriptions are part of the \de\cas\open\server\api\types\DataObjectDescriptionTransferable.
     *        \de\cas\open\server\api\types\ResponseObject: Returns the a list of LinkDescriptions for the given object type.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetLinkDescriptionsRequest
     *	@see \de\cas\open\server\api\types\DataObjectDescriptionTransferable
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see LinkDescription
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetLinkDescriptionsRequest
     */
    class GetLinkDescriptionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $linkDescriptions;

    }

}
