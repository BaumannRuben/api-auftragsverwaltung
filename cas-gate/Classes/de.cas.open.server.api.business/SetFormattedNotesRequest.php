<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\RequestObject: Sets the html notes for the dataobject with the
     *        given GGUID and field name. Use with null content to delete the notes.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: SetFormattedNotesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetFormattedNotesResponse
     */
    class SetFormattedNotesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The object type of the dataobject.
         */
        public $ObjectType;

        /**
         * @var string
         *
         *                    The GGUID of the dataobject.
         */
        public $GGUID;

        /**
         * @var string
         *
         *                    The fieldname of the notes.
         */
        public $FieldName;

        /**
         * @var int
         *
         *                    Determines what should be filtered(IFRAME, onClick, IMG
         *                    src="...", ...). Please see \de\cas\eim\api\constants\HtmlFilterConstants.
         *	@see \de\cas\eim\api\constants\HtmlFilterConstants
         */
        public $FilterValue;

        /**
         * @var unknown
         *File content
         */
        public $fileContent;

        /**
         * @var string
         *Plain text content.
         */
        public $plainContent;

    }

}
