<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Removes items from a distribution list.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: DeleteItemsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteItemsRequest
     */
    class DeleteItemsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										True if at least one address could be deleted, otherwise false.
         */
        public $result;

    }

}
