<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the \de\cas\open\server\business\BusinessOperation
     *				that returns the date when the last password change was made by a user.
     * 				Corresponding \de\cas\open\server\api\types\RequestObject: GetPasswordLastChangedOnRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPasswordLastChangedOnRequest
     */
    class GetPasswordLastChangedOnResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										List of user IDs.
         */
        public $userOids;

        /**
         * @var array
         *
         *										When was the last password change made by a user.
         *										Sorted the same way like userOids.
         */
        public $lastChangedOn;

    }

}
