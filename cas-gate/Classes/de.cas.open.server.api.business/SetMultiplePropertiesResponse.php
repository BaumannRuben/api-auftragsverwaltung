<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Does nothing. Corresponding \de\cas\open\server\api\types\RequestObject:
     *				SetMultiplePropertiesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SetMultiplePropertiesRequest
     */
    class SetMultiplePropertiesResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
