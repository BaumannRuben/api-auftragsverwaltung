<?php
// This file has been automatically generated.

namespace de\cas\open\server\csvexport\types {

    /**
     * @package de\cas\open\server\csvexport
     * @subpackage types
     *Abstract base class for \de\cas\open\server\api\types\ResponseObjects of BOs to export data as CSV.
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    abstract class AbstractExportAsCSVResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *Sets/Returns the csv content.
         */
        public $dataFile;

    }

}
