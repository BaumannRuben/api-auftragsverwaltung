<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Adds multiple addresses to a distribution list.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: AddItemsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AddItemsRequest
     */
    class AddItemsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										True if at least one address could be added, otherwise false.
         */
        public $result;

    }

}
