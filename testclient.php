<?php
// hello_client.php
$options = array(
    'uri' => 'http://soapavfunctions',
    'location' => 'http://192.168.40.33:8083/soapavfunctions.php',
    'trace' => 1
);

$client = new SoapClient(null, $options);

//$result = $client->createAddress('FBBC66D5E1774C4891B91EF4EB560938', 'männlich', 'Max', 'Mustermann', 'mustermann@max.de', '0726351255', '01255424255', 'Chef', 'Oberchef');
$result = $client->createAddress('BF8B3248E68048459AEDDEF460A8C1B4', 'männlich', 'Max', 'Mustermann', 'mustermann@max.de', '0726351255', '01255424255', 'Chef', 'Oberchef');


if(is_soap_fault($result))
{
    print_r ("Fehlercode:   $result->faultcode | Fehlerstring:    $result->faultstring");
}
else
{
    print_r ($result);
}