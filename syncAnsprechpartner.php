<pre>
<?PHP
include 'cas-gate/CASGate.php';
use de\cas\open\server\addresses\types\GetImageForContactResponse;

$datei = fopen("ansprechpartner_logs\\protokoll_".date("Y-m-d-H-i-s").".txt","w");
//print_r(phpinfo());

/* Datenbankverbindung herstellen */
$con = new mysqli('192.168.40.13', 'auftrag', 'Root4Auftrag!', 'hochwarth-zeit');
//$con = new mysqli('localhost', 'root', '', 'hochwarth-zeit');
$con->set_charset('utf8');
if ($con->connect_errno) {
    fwrite($datei,  "Verbindung fehlgeschlagen!\r\n");
    fclose($datei);
    die("Verbindung fehlgeschlagen: " . $con->connect_error);
} else {
    fwrite($datei,  "Verbindung erfolgreich hergestellt!\r\n");
    echo "Verbindung erfolgreich hergestellt!<br><br>";
}

//Pr�fung Webservice und Abbruch wenn nicht erreichbar
$webservice = "http://192.168.40.33:8080";
if( !url_test( $webservice ) ) {
    fwrite($datei,  "Verbindung $webservice fehlgeschlagen!\r\n");
    fclose($datei);
    die("Verbindung $webservice fehlgeschlagen!");
}
else {
    fwrite($datei,  "Verbindung $webservice erfolgreich hergestellt!\r\n");
    echo "Verbindung $webservice erfolgreich hergestellt!<br><br>";
	$option_check['fields'] = array('HOCHWARTH_KUNDENNUMMER', 'GGUID');
    $option_check['conditions'] = array('HOCHWARTH_KUNDENNUMMER !=' => '');
    $check = HochwarthIT_CASGate::getAdressDao()->find('first', $option_check);
    if (empty($check)) {
        fwrite($datei,  "Webservice liefert keine Daten!\r\n");
        fclose($datei);
        die("Webservice liefert keine Daten");
    }
    else {
        fwrite($datei,  "Webservice liefert Daten!\r\n");
        echo "Webservice liefert Daten!<br><br>";
    }
}

$anzAvKunden = 0;
$anzAvKundenNr = 0;
$anzApInsert = 0;
$anzApUpdate = 0;
$anzZInsert = 0;
$anzZUpdate = 0;
$anzPicKunden = 0; 
$anzPicKundenFail = 0; 


//Alle AP deaktivieren
$sql_deaktivate = "UPDATE tblkunden_ap SET Deaktiviert = 1 WHERE `CAS-ID` != ''";
$result = $con->query($sql_deaktivate);
if ($result) {
    fwrite($datei,  "Deaktivierung erfolgreich!\r\n");
}
else {
    fwrite($datei,  "Deaktivierung fehlgeschlagen!\r\n");
    fwrite($datei,  "Fehler: ".$con->error."\r\n");
}

$sqlkunden = $con->query('SELECT KundeNr, Logo FROM tblkunden;');
$avkunden = $sqlkunden->fetch_all(MYSQLI_ASSOC);
//print_r($avkunden);
//die();
foreach ($avkunden as $avkunde) {
	$anzAvKunden++;
	if(!empty($avkunde['KundeNr'])) {
	    $anzAvKundenNr++;
		$option_knd['fields'] = array('HOCHWARTH_KUNDENNUMMER', 'COMPNAME', 'GGUID', 'PHONEFIELDSTR10', 'MAILFIELDSTR5');
	    $option_knd['conditions'] = array('HOCHWARTH_KUNDENNUMMER' => $avkunde['KundeNr'], 'AND GWISCOMPANY' => 1, 'AND GWISCONTACT' => 0);
	    $kunde = HochwarthIT_CASGate::getAdressDao()->find('all', $option_knd);
	    if(!empty($kunde)) {
	       echo "Kunde: ".$kunde[0]['COMPNAME']['value']."<br>";
		   fwrite($datei,  "Kunde: ".$kunde[0]['COMPNAME']['value']."\r\n");
		   //Logo des Kunden �bertragen
	       if(!empty($kunde[0]['GWIMAGEGUID']['value'])) {
	           $image = new GetImageForContactResponse();
	           $image = HochwarthIT_CASGate::getAdressDao()->downloadPicture($kunde[0]['GGUID']['value']);
	           $imgData = base64_encode($image->ImageBytes);
	           $imgExtension = $image->ImageFileExtension;
	           if($avkunde['Logo']!=$imgData) {
	               $sql_update_pic = "UPDATE tblkunden SET Logo ='".$imgData."', Logotyp ='".$imgExtension."' "
	                                ."WHERE KundeNr = ".$kunde[0]['HOCHWARTH_KUNDENNUMMER']['value'].";";
	               if ($con->query($sql_update_pic)) {
	                   $anzPicKunden++;
	               }
	               else {
	                   $anzPicKundenFail++;
	               }
	           }
	       }

                // Zentrale speichern
            if (! empty($kunde[0]['PHONEFIELDSTR10']['value']) || ! empty($kunde[0]['MAILFIELDSTR5']['value'])) {
                $sql_query = "SELECT CAS-ID FROM tblkunden_ap WHERE `CAS-ID` = 'ZENTRALE-" . $kunde[0]['HOCHWARTH_KUNDENNUMMER']['value'] . "';";
                $sqlzentrale = $con->query($sql_query);
                $zentrale = $sqlzentrale->fetch_all(MYSQLI_ASSOC);
                if (empty($zentrale)) {
                    // existiert noch nicht in der AV, Neuanlage
                    $sql_insert = "INSERT INTO tblkunden_ap (KundeNr, `CAS-ID`, Name, Telefon, Mail, Deaktiviert) VALUES ('" . $kunde[0]['HOCHWARTH_KUNDENNUMMER']['value'] . "', '" . "ZENTRALE-" . $kunde[0]['HOCHWARTH_KUNDENNUMMER']['value'] . "', '" . "Zentrale" . "', '" . $kunde[0]['PHONEFIELDSTR10']['value'] . "', '" . $kunde[0]['MAILFIELDSTR5']['value'] . "', " . "0);";
                    // echo $sql_insert."<br>";
                    $result = $con->query($sql_insert);
                    if (! $result) {
                        fwrite($datei, "Zentrale-Neuanlage fehlgeschlagen! - ");
                        fwrite($datei, $sql_insert . "\r\n");
                        fwrite($datei, "Fehler: " . $con->error . "\r\n");
                    }
                    echo "Zentrale-Neuanlage!<br>";
					fwrite($datei,  "Zentrale-Neuanlage!\r\n");
                    $anzZInsert ++;
                } else {
                    // existiert, Aktualisierung
                    $sql_update = "UPDATE tblkunden_ap SET " . "Name ='Zentrale', " . "Telefon ='" . $kunde[0]['PHONEFIELDSTR10']['value'] . "', " . "Mail ='" . $kunde[0]['MAILFIELDSTR5']['value'] . "', " . "Deaktiviert = 0 " . "WHERE `CAS-ID` = 'ZENTRALE-" . $kunde[0]['HOCHWARTH_KUNDENNUMMER']['value'] . "';";
                    // echo $sql_update."<br>";
                    $result = $con->query($sql_update);
                    if (! $result) {
                        fwrite($datei, "Zentrale-Aktualisierung fehlgeschlagen! - ");
                        fwrite($datei, $sql_update . "\r\n");
                        fwrite($datei, "Fehler: " . $con->error . "\r\n");
                    }
                    echo "Zentrale-Aktualisierung!<br>";
					fwrite($datei,  "Zentrale-Aktualisierung!\r\n");
                    $anzZUpdate ++;
                }
            }
	       
	       
	       
	       //Ansprechpartner laden
	       $option_ap['fields'] = array('GGUID', 'NAME', 'CHRISTIANNAME', 'GWIMAGEGUID', 'EINLADEN', 'E_FOKUSKONTAKT', 'E_ITKONTAKT', 'PHONEFIELDSTR4', 'PHONEFIELDSTR2', 'MAILFIELDSTR1', 'DEPARTMENT', 'GWDEPARTMENT2', 'FUNCTION', 'TITLE');
		   $option_ap['conditions'] = array('PRIMARYORGANISATION' => '0x'.$kunde[0]['GGUID']['value'], 'AND GWISCOMPANY' => 0, 'AND GWISCONTACT' => 1, 'AND GWDEACTIVATED' => 0);
	       $ansprechpartner = HochwarthIT_CASGate::getAdressDao()->find('all', $option_ap);
	       for($i=0;$i<sizeof($ansprechpartner);$i++) {
	           
// 	           echo $ansprechpartner[$i]['NAME']['value']." ".$ansprechpartner[$i]['CHRISTIANNAME']['value']." ".$ansprechpartner[$i]['GWIMAGEGUID']['value']."<br>";
	           fwrite($datei,  $ansprechpartner[$i]['NAME']['value']." ".$ansprechpartner[$i]['CHRISTIANNAME']['value']." ".$ansprechpartner[$i]['GWIMAGEGUID']['value']."\r\n");
	           //Bild laden
	           $imgData = "";
	           $imgExtension = "";
	           if(!empty($ansprechpartner[$i]['GWIMAGEGUID']['value'])) {
	               $image = new GetImageForContactResponse();
	               $image = HochwarthIT_CASGate::getAdressDao()->downloadPicture($ansprechpartner[$i]['GGUID']['value']);
	               $imgData = base64_encode($image->ImageBytes);
	               $imgExtension = $image->ImageFileExtension;
	           }
			   if($ansprechpartner[$i]['EINLADEN']['value'])
			       $einladen = 1;
			   else
				   $einladen = 0;
			   if($ansprechpartner[$i]['E_FOKUSKONTAKT']['value'])
			       $fokuskontakt = 1;
			   else
				   $fokuskontakt = 0;
	           if($ansprechpartner[$i]['E_ITKONTAKT']['value'])
			       $itkontakt = 1;
			   else
				   $itkontakt = 0;
	           
	           //Existenz des AP in der AV pr�fen
	           $sql_query = "SELECT CAS-ID FROM tblkunden_ap WHERE `CAS-ID` = '".$ansprechpartner[$i]['GGUID']['value']."';";
	           $sqlap = $con->query($sql_query);
               $ap = $sqlap->fetch_all(MYSQLI_ASSOC);
			   
	           if(empty($ap)) {
	               //existiert noch nicht in der AV, Neuanlage
				   
	               $sql_insert = "INSERT INTO tblkunden_ap (KundeNr, `CAS-ID`, Vorname, Name, Telefon, Mobil, Mail, Abteilung, Abteilung_intern, Funktion, Titel, Bild, Bildtyp, Kommunikationskontakt, Focuskontakt, ITKontakt, Deaktiviert) VALUES ('"
	                           .$kunde[0]['HOCHWARTH_KUNDENNUMMER']['value']."', '" 
	                           .$ansprechpartner[$i]['GGUID']['value']."', '"
	                           .$ansprechpartner[$i]['CHRISTIANNAME']['value']."', '"
	                           .str_replace("'", "\'", $ansprechpartner[$i]['NAME']['value'])."', '"
	                           .$ansprechpartner[$i]['PHONEFIELDSTR4']['value']."', '"
	                           .$ansprechpartner[$i]['PHONEFIELDSTR2']['value']."', '"
	                           .$ansprechpartner[$i]['MAILFIELDSTR1']['value']."', '"
	                           .$ansprechpartner[$i]['DEPARTMENT']['value']."', '"
							   .$ansprechpartner[$i]['GWDEPARTMENT2']['value']."', '"
	                           .$ansprechpartner[$i]['FUNCTION']['value']."', '"
							   .$ansprechpartner[$i]['TITLE']['value']."', '"
	                           .$imgData."', '"
	                           .$imgExtension."', "
							   .$einladen.", "
							   .$fokuskontakt.", "
							   .$itkontakt.", "
	                           ."0);";
 	               echo $sql_insert."<br>";
	               $result = $con->query($sql_insert);
	               if (!$result) {
	                   fwrite($datei,  "AP-Neuanlage fehlgeschlagen! - ");
	                   fwrite($datei,  $ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."\r\n");
	                   fwrite($datei,  $sql_insert."\r\n");
	                   fwrite($datei,  "Fehler: ".$con->error."\r\n");
	               }
	               echo "AP-Neuanlage: ".$ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."<br>";
				   fwrite($datei,  "AP-Neuanlage: ".$ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."\r\n");
	               $anzApInsert++;
	           }
	           else {
	               //existiert, Aktualisierung
	               $sql_update = "UPDATE tblkunden_ap SET "
	                    ."Vorname ='".$ansprechpartner[$i]['CHRISTIANNAME']['value']."', "
	                    ."Name ='".str_replace("'", "\'", $ansprechpartner[$i]['NAME']['value'])."', "
	                    ."Anrede ='".$ansprechpartner[$i]['ADDRESSTERM']['value']."', "
	                    ."Telefon ='".$ansprechpartner[$i]['PHONEFIELDSTR4']['value']."', "
	                    ."Mobil ='".$ansprechpartner[$i]['PHONEFIELDSTR2']['value']."', "
	                    ."Mail ='".$ansprechpartner[$i]['MAILFIELDSTR1']['value']."', "
	                    ."Abteilung ='".$ansprechpartner[$i]['DEPARTMENT']['value']."', "
						."Abteilung_intern ='".$ansprechpartner[$i]['GWDEPARTMENT2']['value']."', "
	                    ."Funktion ='".$ansprechpartner[$i]['FUNCTION']['value']."', "
						."Titel ='".$ansprechpartner[$i]['TITLE']['value']."', "
	                    ."Bild ='".$imgData."', "
	                    ."Bildtyp ='".$imgExtension."', "
						."Kommunikationskontakt = ".$einladen.", "
						."Focuskontakt = ".$fokuskontakt.", "
						."ITKontakt = ".$itkontakt.", "
	                    ."Deaktiviert = 0 "
	                    ."WHERE `CAS-ID` = '".$ansprechpartner[$i]['GGUID']['value']."';";
 	               echo $sql_update."<br>";
	               $result = $con->query($sql_update);
	               if (!$result) {
	                   fwrite($datei,  "AP-Aktualisierung fehlgeschlagen! - ");
	                   fwrite($datei,  $ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."\r\n");
	                   fwrite($datei,  $sql_update."\r\n");
	                   fwrite($datei,  "Fehler: ".$con->error."\r\n");
	               }
	               echo "AP-Aktualisierung: ".$ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."<br>";
				   fwrite($datei,  "AP-Aktualisierung: ".$ansprechpartner[$i]['NAME']['value'].", ".$ansprechpartner[$i]['CHRISTIANNAME']['value']."\r\n");
	               $anzApUpdate++;
	           }
	       }
       }
	}
}

//Deaktivierte AP l�schen
/*$sql_delete = "DELETE FROM tblkunden_ap WHERE Deaktiviert = 1";
$result = $con->query($sql_delete);
if ($result) {
    fwrite($datei,  "L�schung erfolgreich!\r\n");
}
else {
    fwrite($datei,  "L�schung fehlgeschlagen!\r\n");
    fwrite($datei,  "Fehler: ".$con->error."\r\n");
}*/

echo "Kunden: ".$anzAvKunden."<br>";
echo "Kunden mit KundeNr: ".$anzAvKundenNr."<br>";
echo "AP-Neuanlage: ".$anzApInsert."<br>";
echo "AP-Aktualisierung: ".$anzApUpdate."<br>";
echo "Kunden mit Bild: ".$anzPicKunden."<br>";
echo "Kunden mit Bild Fehler: ".$anzPicKundenFail."<br>";
fwrite($datei,  "Kunden: $anzAvKunden\r\n");
fwrite($datei,  "Kunden mit KundeNr: $anzAvKundenNr\r\n");
fwrite($datei,  "AP-Neuanlage: $anzApInsert\r\n");
fwrite($datei,  "AP-Aktualisierung: $anzApUpdate\r\n");
fwrite($datei,  "Kunden mit Bild: $anzPicKunden\r\n");
fwrite($datei,  "Kunden mit Bild Fehler: $anzPicKundenFail\r\n");
fclose($datei);



$con->close();


//Pr�fung URL
function url_test( $url ) {
    $timeout = 10;
    $ch = curl_init();
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
    $http_respond = curl_exec($ch);
    $http_respond = trim( strip_tags( $http_respond ) );
    $http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
    if ( ( $http_code == "200" ) || ( $http_code == "302" ) ) {
        return true;
    } else {
        //return $http_code;
        return false;
    }
    curl_close( $ch );
}
?>
</pre>