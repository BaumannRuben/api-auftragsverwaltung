<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Requests the default language set for the current client
     *				to be returned. Corresponding \de\cas\open\server\api\types\ResponseObject: GetSystemDefaultLanguageResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSystemDefaultLanguageResponse
     */
    class GetSystemDefaultLanguageRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
