<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Sets (creates/updates) the given \de\cas\open\server\api\types\DataObjects of the
     *        given object type as dependent \de\cas\open\server\api\types\DataObjects of the object
     *        that is identified by the given superior object type and
     *        superior object guid. Corresponding \de\cas\open\server\api\types\RequestObject: SetDependentObjectsRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SetDependentObjectsRequest
     */
    class SetDependentObjectsResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
