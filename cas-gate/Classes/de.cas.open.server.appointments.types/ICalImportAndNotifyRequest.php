<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *        Request Object to import an iCal and handle the command.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        ICalImportAndNotifyResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ICalImportAndNotifyResponse
     */
    class ICalImportAndNotifyRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Content of the iCal-Source as string
         */
        public $iCalSource;

        /**
         * @var string
         *
         *                    E-mail address of the user which imports the iCal-Source.
         *                    The users e-mail and the e-mail in the iCal-Source have to match, otherwise an error occurs
         */
        public $email;

        /**
         * @var string
         *
         *                    Commands for iCal-Request and cancellation commands to notify the external organizer or import a cancellation
         */
        public $ICSCommand;

    }

}
