<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        ResponseObject which returns all available licence module names of an user.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetLicensedModulesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetLicensedModulesRequest
     */
    class GetLicensedModulesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Contains licence module names.
         */
        public $availableModules;

    }

}
