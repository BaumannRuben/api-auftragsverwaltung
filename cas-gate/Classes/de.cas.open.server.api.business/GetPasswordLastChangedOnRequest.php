<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the \de\cas\open\server\business\BusinessOperation that
     * 				returns the date when the last password change was made by a user.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetPasswordLastChangedOnResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPasswordLastChangedOnResponse
     */
    class GetPasswordLastChangedOnRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Get dates of these users. If the list is empty, the dates of all users are returned.
         */
        public $userOids;

    }

}
