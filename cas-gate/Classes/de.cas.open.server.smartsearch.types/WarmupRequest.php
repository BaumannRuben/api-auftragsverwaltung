<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Initiates a warmup of the search index for the current user.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: WarmupResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see WarmupResponse
     */
    class WarmupRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
