<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Retrieves the object types a group is available for.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetObjectTypesForTagGroupRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetObjectTypesForTagGroupRequest
     */
    class GetObjectTypesForTagGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Gets the available object types, if any are available.
         */
        public $objectTypes;

    }

}
