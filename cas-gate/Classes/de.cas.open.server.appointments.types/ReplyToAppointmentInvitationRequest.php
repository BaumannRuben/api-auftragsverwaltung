<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *        Request Object to reply to an invitation to an appointment.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        ReplyToAppointmentInvitationResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ReplyToAppointmentInvitationResponse
     */
    class ReplyToAppointmentInvitationRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The GUID of the appointment to which the user is invited.
         */
        public $GUID;

        /**
         * @var string
         *
         *                    Commands to reply to the invitation (ACCEPTED/TENTATIVE/DECLINED) and notify the external organizer.
         */
        public $ICSCommand;

    }

}
