<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        Formats the phone number according to the given country code.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: FormatPhoneNumberResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see FormatPhoneNumberResponse
     */
    class FormatPhoneNumberRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The phone number that needs to be formatted.
         */
        public $phoneNumber;

        /**
         * @var string
         *
         *                    The ISO code of the country (e.g. DE, CH). If it's not filled default will be used when available.
         */
        public $countryCode;

    }

}
