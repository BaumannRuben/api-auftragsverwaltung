<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that removes all properties that
     *				belong to the given propertyGroup.
     */
    class RemovePropertiesByGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $propertyGroup;

    }

}
