<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Adds multiple addresses to a distribution list.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: AddItemsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AddItemsResponse
     */
    class AddItemsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the distribution list record.
         */
        public $DistributionListGGUID;

        /**
         * @var array
         *
         *										GGUID of the address records.
         */
        public $AddressGGUIDs;

    }

}
