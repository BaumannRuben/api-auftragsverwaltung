<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the available translations in one language for a field input item.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetFieldInputTranslationsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFieldInputTranslationsRequest
     */
    class GetFieldInputTranslationsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    A list of key-value pairs with the string in the source language as key
         *                    and the corresponding translation in the destination language as value.
         */
        public $translations;

    }

}
