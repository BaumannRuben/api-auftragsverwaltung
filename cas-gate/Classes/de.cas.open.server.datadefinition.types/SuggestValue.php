<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class SuggestValue {

        /**
         * @var array
         *
         *                GGUIDs of user groups that have permissions to the SuggestValue
         */
        public $GroupGuids;

    }

}
