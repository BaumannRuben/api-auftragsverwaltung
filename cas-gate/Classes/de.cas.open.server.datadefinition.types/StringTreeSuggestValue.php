<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class StringTreeSuggestValue extends \de\cas\open\server\datadefinition\types\StringSuggestValue {

        /**
         * @var array
         *
         *                    List of child suggestValues
         */
        public $ChildSuggestValues;

    }

}
