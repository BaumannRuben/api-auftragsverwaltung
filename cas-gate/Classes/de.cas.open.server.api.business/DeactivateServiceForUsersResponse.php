<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> deactivate external service for users.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: DeactivateServiceForUsersRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeactivateServiceForUsersRequest
     */
    class DeactivateServiceForUsersResponse extends \de\cas\open\server\api\business\EmptyResponse {

    }

}
